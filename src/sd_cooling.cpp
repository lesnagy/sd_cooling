//
// Created by L. Nagy on 30/11/2020.
//

#include <boost/python.hpp>
#include <boost/python/numpy.hpp>

#include "version.hpp"

#include "temperature.hpp"
#include "sd_cooling.hpp"

using namespace boost::python;
using namespace boost::python::numpy;

// Wrapper for linear temperature function.
boost::python::object get_linear_temperature_function_pywrapper(Real t0, Real T0, Real t1, Real T1) {
    auto func = linear_temperature_function(t0, T0, t1, T1);
    auto call_policies = boost::python::default_call_policies();
    typedef boost::mpl::vector<Real, Real> func_sig;
    return boost::python::make_function(func, call_policies, func_sig());
}

// Wrapper for a Newtonian temperature function.
boost::python::object get_newtonian_temperature_function_pywrapper(Real Tamb, Real T0, Real t1, Real T1) {
    auto func = newtonian_temperature_function(Tamb, T0, t1, T1);
    auto call_policies = boost::python::default_call_policies();
    typedef boost::mpl::vector<Real, Real> func_sig;
    return boost::python::make_function(func, call_policies, func_sig());
}

// Wrapper for a Newtonian temperature rate of change function.
boost::python::object get_d_newtonian_temperature_function_dt_pywrapper(Real Tamb, Real T0, Real t1, Real T1) {
  auto func = d_newtonian_temperature_function_dt(Tamb, T0, t1, T1);
  auto call_policies = boost::python::default_call_policies();
  typedef boost::mpl::vector<Real, Real> func_sig;
  return boost::python::make_function(func, call_policies, func_sig());
}

// Returns git versioning info.
const char* version_info() {
  return GIT_VERSION;
}

BOOST_PYTHON_MODULE(sd_coolingc)
{
  	//
  	// Expose version function to python.
  	//
  	def("version_info", version_info);

    //
    // Expose temperature functions to python.
    //

    def("linear_temperature_function", get_linear_temperature_function_pywrapper);
    def("newtonian_temperature_function", get_newtonian_temperature_function_pywrapper);
  	def("d_newtonian_temperature_function_dt", get_d_newtonian_temperature_function_dt_pywrapper);

    //
    // Expose the ProjectionType enumeration.
    //
    enum_<ProjectionType>("projection")
            .value("none", NONE)
            .value("parallel", PARALLEL)
            .value("perpendicular", PERPENDICULAR)
            .export_values()
            ;

    //
    // Expose the 'Assemblage' class to python.
    //

    // Assign local variables for overloaded functions.
    void (Assemblage::*update_field_strength)(Real H) = &Assemblage::update_field;
    void (Assemblage::*update_field)(Real H, Real hx, Real hy, Real hz) = &Assemblage::update_field;

    // Expose assemblage class.
    class_<Assemblage>("Assemblage", init<Real, Real, ISize>())
            .def("add_stoner_wohlfarth_population", &Assemblage::add_stoner_wohlfarth_population)
            .def("update_field_strength", update_field_strength)
            .def("update_field", update_field)
            .def("update_temperature", &Assemblage::update_temperature_and_rho)
            .def("update_temperature_and_rho", &Assemblage::update_temperature_and_rho)
            .def("equilibriate", &Assemblage::equilibriate)
            .def("population_magnetization", &Assemblage::population_magnetization)
            .def("equilibrium_magnetization", &Assemblage::equilibrium_magnetization)
            ;

    //class_<Vector2D>("Vector2D", init());
    class_<Vector3D>("Vector3D", init())
            .def("norm", &Vector3D::norm)
            ;
}

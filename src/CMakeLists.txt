#
# Greeter python test library.
#

message("BOOST LIBRARIES: ${Boost_LIBRARIES}")
message("PYTHON LIBRARIES: ${PYTHON_LIBRARIES}")

add_library(greeterc SHARED greeter.cpp)
target_link_libraries(greeterc
        ${Boost_LIBRARIES}
        ${PYTHON_LIBRARIES})

if (WIN32)
    add_custom_command(TARGET greeterc POST_BUILD
            COMMAND ${CMAKE_COMMAND} -E copy
            ${CMAKE_CURRENT_BINARY_DIR}/greeterc.dll
            ${CMAKE_CURRENT_BINARY_DIR}/greeterc.pyd)
elseif(UNIX)
    add_custom_command(TARGET greeterc POST_BUILD
            COMMAND ${CMAKE_COMMAND} -E copy
            ${CMAKE_CURRENT_BINARY_DIR}/libgreeterc.so
            ${CMAKE_CURRENT_BINARY_DIR}/greeterc.so)
endif()

#
# Single domain cooling python library.
#

add_library(sd_coolingc SHARED sd_cooling.cpp)
target_link_libraries(sd_coolingc
        ${Boost_LIBRARIES}
        ${PYTHON_LIBRARIES})
if (WIN32)
    add_custom_command(TARGET sd_coolingc POST_BUILD
            COMMAND ${CMAKE_COMMAND} -E copy
            ${CMAKE_CURRENT_BINARY_DIR}/sd_coolingc.dll
            ${CMAKE_CURRENT_BINARY_DIR}/sd_coolingc.pyd)
elseif(UNIX)
    add_custom_command(TARGET sd_coolingc POST_BUILD
            COMMAND ${CMAKE_COMMAND} -E copy
            ${CMAKE_CURRENT_BINARY_DIR}/libsd_coolingc.so
            ${CMAKE_CURRENT_BINARY_DIR}/sd_coolingc.so)
endif()
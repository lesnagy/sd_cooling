//
// Created by L. Nagy on 18/11/2020.
//

#include <iostream>

#include <boost/python.hpp>

void speak()
{
    std::cout << "Hello python, this is C++! Here is some useful information ..." << std::endl;
    std::cout << "size of 'double':      " << sizeof(double) << std::endl;
    std::cout << "size of 'long double': " << sizeof(long double) << std::endl;
    std::cout << "size of 'float':       " << sizeof(float) << std::endl;
}

BOOST_PYTHON_MODULE(greeterc)
{
    boost::python::def("speak", speak);
}
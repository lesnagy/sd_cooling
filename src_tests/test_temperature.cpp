//
// Created by L. Nagy on 01/12/2020.
//


#include <iostream>
#include <iomanip>
#include <fstream>

#include "gtest/gtest.h"
#include "gtest_macros.hpp"

#include "temperature.hpp"

TEST(linear_temperature_function, scenario_one) {
    using namespace std;

    // With linear cooling.
    Real t0 = 0.0;
    Real T0 = 579.0;
    Real t1 = 10.0;
    Real T1 = 30.00;

    auto tfun = linear_temperature_function(t0, T0, t1, T1);

    // Run the cooling model.
    int nsteps = 1000;
    Real dt = t1/(nsteps - 1);

    // Test against a file generated using mathematica.
    std::fstream expected_data("D:\\projects\\sd_cooling\\test_data\\linear_temperature.csv");

    // Loop the model.
    if (expected_data.is_open()) {
        for (Index i = 0; i < nsteps; ++i) {

            std::string line;
            std::getline(expected_data, line);

            auto expected = str_split_to_number(line);

            Real t = 0.0 + dt * i;
            Real T = tfun(t);

            Real eps = 1E-12;

            ASSERT_NEAR(expected[0], t, eps);
            ASSERT_NEAR(expected[1], T, eps);
        }
    } else {
        FAIL() << "Couldn't open test file.";
    }

    expected_data.close();
}

TEST(newtonian_temperature_function, scenario_one) {
    using namespace std;

    // With linear cooling.
    Real Tamb = 15.00;
    Real T0 = 579.0;
    Real t1 = 10.0;
    Real T1 = 30.00;

    auto tfun = newtonian_temperature_function(Tamb, T0, T1, t1);

    // Run the cooling model.
    int nsteps = 1000;
    Real dt = t1/(nsteps - 1);

    // Test against a file generated using mathematica.
    std::fstream expected_data("D:\\projects\\sd_cooling\\test_data\\newtonian_temperature.csv");

    // Loop the model.
    if (expected_data.is_open()) {
        for (Index i = 0; i < nsteps*4; ++i) {

            std::string line;
            std::getline(expected_data, line);

            auto expected = str_split_to_number(line);

            Real t = 0.0 + dt * i;
            Real T = tfun(t);

            Real eps = 1E-12;

            ASSERT_NEAR(expected[0], t, eps);
            ASSERT_NEAR(expected[1], T, eps);
        }
    } else {
        FAIL() << "Couldn't open test file.";
    }

    expected_data.close();
}


TEST(newtonian_temperature_function, scenario_two) {
    using namespace std;

    // With linear cooling.
    Real Tamb = 15.00;
    Real T0 = 579.0;
    Real t1 = 10.0;
    Real T1 = 30.00;

    auto tfun = newtonian_temperature_function(Tamb, T0, T1, t1);

    cout << tfun(20.0) << endl;

}
//
// Created by L. Nagy on 22/10/2020.
//

#include <iostream>
#include <iomanip>
#include <fstream>

#include "gtest/gtest.h"
#include "gtest_macros.hpp"

#include "temperature.hpp"
#include "stoner_wohlfarth.hpp"


TEST(Na, normal_operation) {
    using namespace std;

    vector< array<Real, 4> > Na_expected = {
        {1.0500,5.0000,0.32041622404105092860,0.33979188797947453570},
        {1.1000,10.000,0.30828483392095208783,0.34585758303952395608},
        {1.1500,15.000,0.29687459569712485551,0.35156270215143757224},
        {1.2000,20.000,0.28612757240672259371,0.35693621379663870314},
        {1.2500,25.000,0.27599164610354165753,0.36200417694822917123},
        {1.3000,30.000,0.26641982113574917481,0.36679008943212541260},
        {1.3500,35.000,0.25736962374476102487,0.37131518812761948756},
        {1.4000,40.000,0.24880258295696291542,0.37559870852151854229},
        {1.4500,45.000,0.24068378034123731305,0.37965810982938134348},
        {1.5000,50.000,0.23298145831360969333,0.38350927084319515333},
        {1.5500,55.000,0.22566667838693584894,0.38716666080653207553},
        {1.6000,60.000,0.21871302216729203546,0.39064348891635398227},
        {1.6500,65.000,0.21209632905141515593,0.39395183547429242204},
        {1.7000,70.000,0.20579446552986011815,0.39710276723506994093},
        {1.7500,75.000,0.19978712178703390234,0.40010643910648304883},
        {1.8000,80.000,0.19405563194257645295,0.40297218402871177352},
        {1.8500,85.000,0.18858281482316634019,0.40570859258841682990},
        {1.9000,90.000,0.18335283260937191367,0.40832358369531404316},
        {1.9500,95.000,0.17835106508446560064,0.41082446745776719968},
        {2.0000,100.00,0.17356399753396423169,0.41321800123301788416}
    };

    for (size_t i = 0; i < 20; ++i) {
        long double m = 1.0500 + 0.05*i;
        ASSERT_DOUBLE_EQ(m, Na_expected[i][0]);
        ASSERT_NEAR(Na(m), Na_expected[i][2], 1E-14);
    }
}

TEST(Nb, normal_operation) {
    using namespace std;

    vector< array<Real, 4> > Nb_expected = {
        {1.0500,5.0000,0.32041622404105092860,0.33979188797947453570},
        {1.1000,10.000,0.30828483392095208783,0.34585758303952395608},
        {1.1500,15.000,0.29687459569712485551,0.35156270215143757224},
        {1.2000,20.000,0.28612757240672259371,0.35693621379663870314},
        {1.2500,25.000,0.27599164610354165753,0.36200417694822917123},
        {1.3000,30.000,0.26641982113574917481,0.36679008943212541260},
        {1.3500,35.000,0.25736962374476102487,0.37131518812761948756},
        {1.4000,40.000,0.24880258295696291542,0.37559870852151854229},
        {1.4500,45.000,0.24068378034123731305,0.37965810982938134348},
        {1.5000,50.000,0.23298145831360969333,0.38350927084319515333},
        {1.5500,55.000,0.22566667838693584894,0.38716666080653207553},
        {1.6000,60.000,0.21871302216729203546,0.39064348891635398227},
        {1.6500,65.000,0.21209632905141515593,0.39395183547429242204},
        {1.7000,70.000,0.20579446552986011815,0.39710276723506994093},
        {1.7500,75.000,0.19978712178703390234,0.40010643910648304883},
        {1.8000,80.000,0.19405563194257645295,0.40297218402871177352},
        {1.8500,85.000,0.18858281482316634019,0.40570859258841682990},
        {1.9000,90.000,0.18335283260937191367,0.40832358369531404316},
        {1.9500,95.000,0.17835106508446560064,0.41082446745776719968},
        {2.0000,100.00,0.17356399753396423169,0.41321800123301788416}
    };

    for (size_t i = 0; i < 20; ++i) {
        long double m = 1.0500 + 0.05*i;
        ASSERT_DOUBLE_EQ(m, Nb_expected[i][0]);
        ASSERT_NEAR(Nb(m), Nb_expected[i][3], 1E-14);
    }
}


TEST(StonerWohlfarthGrainPopulation, temperature_evolution_para) {
    using namespace std;

    Real esvd = 40E-9;
    Real elong = 30.0;
    Real H = 23.87;
    string material = "magnetite";

    // With linear cooling.
    Real Tamb = 15.00;
    Real T0 = 579.0;
    Real t1 = 10.0;
    Real T1 = 30.00;

    auto tempfun = newtonian_temperature_function(Tamb, T0, T1, t1);
    auto ms = saturation_magnetization_function(name_to_material(material));
    auto v = (pi/6.0) * esvd * esvd * esvd;

    auto grain_pop = StonerWohlfarthGrainPopulation(
            esvd, elong,               // geometry
            H, 1.0, 2.0, 8.1,          // applied field
            1.0, 2.0, 8.0,             // grain orientation
            material,                  // material name
            T0, {1.0, 0.0}, 1E-12, 0); // start temperature, population, epsilon value and no. of polish steps

    auto init_mag = grain_pop.population_magnetization();

    // Run the cooling model.
    ISize nsteps = 10000;
    Real dt = t1/(nsteps - 1);

    // Test against a file generated using mathematica.
    std::fstream expected_data("D:\\projects\\sd_cooling\\test_data\\sw_parallel_mathematica.csv");

    // Loop the model.
    if (expected_data.is_open()) {
        for (Index i = 0; i < nsteps; ++i) {

            std::string line;
            std::getline(expected_data, line);

            auto expected = str_split_to_number(line);

            Real t = 0.0 + dt * i;
            Real T = tempfun(t);

            grain_pop.update_temperature(T);
            grain_pop.update_rho(dt);

            auto rho = grain_pop.get_rho();

            auto mags = grain_pop.population_magnetization(NONE, false);
            auto magsPara = grain_pop.population_magnetization(PARALLEL, false);
            auto magsPerp = grain_pop.population_magnetization(PERPENDICULAR, false);

            auto eqMags = grain_pop.equilibrium_magnetization(NONE, false);
            auto eqMagsPara = grain_pop.equilibrium_magnetization(PARALLEL, false);
            auto eqMagsPerp = grain_pop.equilibrium_magnetization(PERPENDICULAR, false);

            auto meq2 = tanh((mu0 * v * ms(T) * H) / (kb * (T + 273.15)));

            Real eps = 1E-9;
            ASSERT_NEAR(expected[0], t, eps);
            ASSERT_NEAR(expected[1], T, eps);
            ASSERT_NEAR(expected[2], mags.norm(), eps);
            ASSERT_NEAR(expected[3], magsPara.norm(), eps);
            ASSERT_NEAR(expected[4], magsPerp.norm(), eps);
            ASSERT_NEAR(expected[5], eqMags.norm(), eps);
            ASSERT_NEAR(expected[6], eqMagsPara.norm(), eps);
            ASSERT_NEAR(expected[7], eqMagsPerp.norm(), eps);
        }
    } else {
        FAIL() << "Couldn't open test file.";
    }
}


TEST(StonerWohlfarthGrainPopulation, temperature_evolution_perp) {
    using namespace std;

    Real esvd = 40E-9;
    Real elong = 30.0;
    Real H = 23.87;
    string material = "magnetite";

    // With linear cooling.
    Real Tamb = 15.00;
    Real T0 = 579.0;
    Real t1 = 10.0;
    Real T1 = 30.00;

    auto tempfun = newtonian_temperature_function(Tamb, T0, T1, t1);
    auto ms = saturation_magnetization_function(name_to_material(material));
    auto v = (pi/6.0) * esvd * esvd * esvd;

    auto grain_pop = StonerWohlfarthGrainPopulation(
            esvd, elong,               // geometry
            H, 1.0, 2.0, -2,           // applied field
            1.0, 2.0, 8.0,             // grain orientation
            material,                  // material name
            T0, {1.0, 0.0}, 1E-12, 0); // start temperature, population, epsilon value and no. of polish steps

    auto init_mag = grain_pop.population_magnetization();

    // Run the cooling model.
    ISize nsteps = 10000;
    Real dt = t1/(nsteps - 1);

    // Test against a file generated using Mathematica.
    std::fstream expected_data("D:\\projects\\sd_cooling\\test_data\\sw_perpendicular_mathematica.csv");

    // Loop the model.
    if (expected_data.is_open()) {
        for (Index i = 0; i < nsteps; ++i) {

            std::string line;
            std::getline(expected_data, line);

            auto expected = str_split_to_number(line);

            Real t = 0.0 + dt * i;
            Real T = tempfun(t);

            grain_pop.update_temperature(T);
            grain_pop.update_rho(dt);

            auto rho = grain_pop.get_rho();

            auto mags = grain_pop.population_magnetization(NONE, false);
            auto magsPara = grain_pop.population_magnetization(PARALLEL, false);
            auto magsPerp = grain_pop.population_magnetization(PERPENDICULAR, false);

            auto eqMags = grain_pop.equilibrium_magnetization(NONE, false);
            auto eqMagsPara = grain_pop.equilibrium_magnetization(PARALLEL, false);
            auto eqMagsPerp = grain_pop.equilibrium_magnetization(PERPENDICULAR, false);

            auto meq2 = tanh((mu0 * v * ms(T) * H) / (kb * (T + 273.15)));

            Real eps = 1E-9;
            ASSERT_NEAR(expected[0], t, eps);
            ASSERT_NEAR(expected[1], T, eps);
            ASSERT_NEAR(expected[2], mags.norm(), eps);
            ASSERT_NEAR(expected[3], magsPara.norm(), eps);
            ASSERT_NEAR(expected[4], magsPerp.norm(), eps);
            ASSERT_NEAR(expected[5], eqMags.norm(), eps);
            ASSERT_NEAR(expected[6], eqMagsPara.norm(), eps);
            ASSERT_NEAR(expected[7], eqMagsPerp.norm(), eps);
        }
    } else {
        FAIL() << "Couldn't open test file.";
    }
}

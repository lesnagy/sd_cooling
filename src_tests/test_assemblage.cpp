//
// Created by L. Nagy on 30/11/2020.
//

#include <iostream>
#include <iomanip>
#include <fstream>

#include "gtest/gtest.h"
#include "gtest_macros.hpp"

#include "temperature.hpp"
#include "sd_cooling.hpp"


TEST(StonerWohlfarthGrainPopulation, temperature_evolution_para) {
    using namespace std;

    Real esvd = 40E-9;
    Real elong = 30.0;
    Real H = 23.87;
    string material = "magnetite";

    // With linear cooling.
    Real Tamb = 15.00;
    Real T0 = 579.0;
    Real t1 = 10.0;
    Real T1 = 30.00;

    auto tempfun = newtonian_temperature_function(Tamb, T0, T1, t1);
    auto ms = saturation_magnetization_function(name_to_material(material));
    auto v = (pi/6.0) * esvd * esvd * esvd;

    // Create an assemblage with an initial temperature of T0.
    auto assemblage = Assemblage(T0);

    // Add an assemblage of 'parallel test grains'.
    assemblage.add_stoner_wohlfarth_population(
            esvd, elong,       // geometry
            H, 1.0, 2.0, 8.1,  // applied field
            1.0, 2.0, 8.0,     // grain orientation
            material,          // material name
            1.0, 0.0, 0.5);  // population, fraction of grains

    // Add another assemblage of exactly the same grains.
    assemblage.add_stoner_wohlfarth_population(
            esvd, elong,       // geometry
            H, 1.0, 2.0, 8.1,  // applied field
            1.0, 2.0, 8.0,     // grain orientation
            material,          // material name
            1.0, 0.0, 0.5);  // population, fraction of grains

    // Run the cooling model.
    ISize nsteps = 10000;
    Real dt = t1/(nsteps - 1);

    // Test against a file generated using mathematica.
    std::fstream expected_data("D:\\projects\\sd_cooling\\test_data\\sw_parallel_mathematica.csv");

    // Loop the model.
    if (expected_data.is_open()) {
        for (Index i = 0; i < nsteps; ++i) {

            std::string line;
            std::getline(expected_data, line);

            auto expected = str_split_to_number(line);

            Real t = 0.0 + dt * i;
            Real T = tempfun(t);

            assemblage.update_temperature_and_rho(T, dt);

            auto mags = assemblage.population_magnetization(NONE, false);
            auto magsPara = assemblage.population_magnetization(PARALLEL, false);
            auto magsPerp = assemblage.population_magnetization(PERPENDICULAR, false);

            auto eqMags = assemblage.equilibrium_magnetization(NONE, false);
            auto eqMagsPara = assemblage.equilibrium_magnetization(PARALLEL, false);
            auto eqMagsPerp = assemblage.equilibrium_magnetization(PERPENDICULAR, false);

            auto meq2 = tanh((mu0 * v * ms(T) * H) / (kb * (T + 273.15)));

            Real eps = 1E-9;
            ASSERT_NEAR(expected[0], t, eps);
            ASSERT_NEAR(expected[1], T, eps);
            ASSERT_NEAR(expected[2], mags.norm(), eps);
            ASSERT_NEAR(expected[3], magsPara.norm(), eps);
            ASSERT_NEAR(expected[4], magsPerp.norm(), eps);
            ASSERT_NEAR(expected[5], eqMags.norm(), eps);
            ASSERT_NEAR(expected[6], eqMagsPara.norm(), eps);
            ASSERT_NEAR(expected[7], eqMagsPerp.norm(), eps);
        }
    }
}

TEST(StonerWohlfarthGrainPopulation, temperature_evolution_perp) {
    using namespace std;

    Real esvd = 40E-9;
    Real elong = 30.0;
    Real H = 23.87;
    string material = "magnetite";

    // With linear cooling.
    Real Tamb = 15.00;
    Real T0 = 579.0;
    Real t1 = 10.0;
    Real T1 = 30.00;

    auto tempfun = newtonian_temperature_function(Tamb, T0, T1, t1);
    auto ms = saturation_magnetization_function(name_to_material(material));
    auto v = (pi/6.0) * esvd * esvd * esvd;

    // Create an assemblage with an initial temperature of T0.
    auto assemblage = Assemblage(T0);

    // Add an assemblage of 'parallel test grains'.
    assemblage.add_stoner_wohlfarth_population(
            esvd, elong,       // geometry
            H, 1.0, 2.0, -2.0, // applied field
            1.0, 2.0, 8.0,     // grain orientation
            material,          // material name
            1.0, 0.0, 0.5);  // population, fraction of grains

    // Add another assemblage of exactly the same grains.
    assemblage.add_stoner_wohlfarth_population(
            esvd, elong,       // geometry
            H, 1.0, 2.0, -2.0,  // applied field
            1.0, 2.0, 8.0,     // grain orientation
            material,          // material name
            1.0, 0.0, 0.5);  // population, fraction of grains

    // Run the cooling model.
    ISize nsteps = 10000;
    Real dt = t1/(nsteps - 1);

    // Test against a file generated using mathematica.
    std::fstream expected_data("D:\\projects\\sd_cooling\\test_data\\sw_perpendicular_mathematica.csv");

    // Loop the model.
    if (expected_data.is_open()) {
        for (Index i = 0; i < nsteps; ++i) {

            std::string line;
            std::getline(expected_data, line);

            auto expected = str_split_to_number(line);

            Real t = 0.0 + dt * i;
            Real T = tempfun(t);

            assemblage.update_temperature_and_rho(T, dt);

            auto mags = assemblage.population_magnetization(NONE, false);
            auto magsPara = assemblage.population_magnetization(PARALLEL, false);
            auto magsPerp = assemblage.population_magnetization(PERPENDICULAR, false);

            auto eqMags = assemblage.equilibrium_magnetization(NONE, false);
            auto eqMagsPara = assemblage.equilibrium_magnetization(PARALLEL, false);
            auto eqMagsPerp = assemblage.equilibrium_magnetization(PERPENDICULAR, false);

            auto meq2 = tanh((mu0 * v * ms(T) * H) / (kb * (T + 273.15)));

            Real eps = 1E-9;
            ASSERT_NEAR(expected[0], t, eps);
            ASSERT_NEAR(expected[1], T, eps);
            ASSERT_NEAR(expected[2], mags.norm(), eps);
            ASSERT_NEAR(expected[3], magsPara.norm(), eps);
            ASSERT_NEAR(expected[4], magsPerp.norm(), eps);
            ASSERT_NEAR(expected[5], eqMags.norm(), eps);
            ASSERT_NEAR(expected[6], eqMagsPara.norm(), eps);
            ASSERT_NEAR(expected[7], eqMagsPerp.norm(), eps);
        }
    }
}

//
// Created by L. Nagy on 21/10/2020.
//

#include "gtest/gtest.h"

#include "constants.hpp"
#include "utilities.hpp"

TEST(sgn, negative_sign) {
    ASSERT_EQ(sgn(-3.423), -1);
}

TEST(sgn, positive_sign) {
    ASSERT_EQ(sgn(3.432), 1);
}

TEST(sgn, zero) {
    ASSERT_EQ(sgn(0.0), 0);
}

TEST(sign_change, no_change) {
    ASSERT_FALSE (
            sign_change(
                    [](long double x) { return x - 1; },
                    0.0,
                    0.5
            )
    );
}

TEST(sign_change, change_negative_to_zero) {
    ASSERT_TRUE (
            sign_change(
                    [](long double x) { return x - 1; },
                    0.0,
                    1.0
            )
    );
}

TEST(sign_change, change_positive_to_zero) {
    ASSERT_TRUE (
            sign_change(
                    [](long double x) { return x - 1; },
                    2.0,
                    1.0
            )
    );
}

TEST(sign_change, change_positive_to_negative) {
    ASSERT_TRUE (
            sign_change(
                    [](long double x) { return x - 1; },
                    2.0,
                    0.0
            )
    );
}

TEST(compute_iter_start_end, n_17_np_3) {
    auto result = compute_iter_start_end(17, 3);
    ASSERT_EQ(result.size(), 3);
    ASSERT_EQ(result[0][0], 0); ASSERT_EQ(result[0][1], 6);
    ASSERT_EQ(result[1][0], 6); ASSERT_EQ(result[1][1], 12);
    ASSERT_EQ(result[2][0], 12); ASSERT_EQ(result[2][1], 17);
}

TEST(compute_iter_start_end, n_17_np_1) {
    auto result = compute_iter_start_end(17, 1);
    ASSERT_EQ(result.size(), 1);
    ASSERT_EQ(result[0][0], 0); ASSERT_EQ(result[0][1], 17);
}

TEST(compute_iter_start_end, n_20000001_np_2) {
    auto result = compute_iter_start_end(20000001, 2);
    ASSERT_EQ(result.size(), 2);
    ASSERT_EQ(result[0][0], 0); ASSERT_EQ(result[0][1], 10000001);
    ASSERT_EQ(result[1][0], 10000001); ASSERT_EQ(result[1][1], 20000001);
}

TEST(to_lowercase, normal_operation) {
    auto result = to_lower_case("Thi{s strING}}!");
    ASSERT_EQ(result, "thi{s string}}!");
}

TEST(rotation_matrix, normal_operation) {
    auto rax = rotation_matrix(Vector3D(1, 3, 4), pi/3.0);

    long double eps = 1E-12;
    ASSERT_NEAR(rax(0, 0),  0.51923076923076923077, eps);
    ASSERT_NEAR(rax(0, 1), -0.62167391279444975806, eps);
    ASSERT_NEAR(rax(0, 2),  0.58644774228814501085, eps);

    ASSERT_NEAR(rax(1, 0),  0.73705852817906514267, eps);
    ASSERT_NEAR(rax(1, 1),  0.67307692307692307692, eps);
    ASSERT_NEAR(rax(1, 2),  0.060927675647541406639, eps);

    ASSERT_NEAR(rax(2, 0), -0.43260158844199116470, eps);
    ASSERT_NEAR(rax(2, 1),  0.40061078589092013182, eps);
    ASSERT_NEAR(rax(2, 2),  0.80769230769230769231, eps);
}

TEST(nearness_sort, normal_operation) {
    using namespace std;

    vector<Real> thetas = {-3.14159, 3.1419, 1.0472, 1.5708, -1.5708, 0.523599, -0.523599, -2.0944, 2.0944, -1.0472};

    nearness_sort(thetas, -0.349066);

    long double eps = 1E-12;
    ASSERT_NEAR(thetas[0], -0.523599, eps);
    ASSERT_NEAR(thetas[1], -1.0472, eps);
    ASSERT_NEAR(thetas[2],  0.523599, eps);
    ASSERT_NEAR(thetas[3], -1.5708, eps);
    ASSERT_NEAR(thetas[4],  1.0472, eps);
    ASSERT_NEAR(thetas[5], -2.0944, eps);
    ASSERT_NEAR(thetas[6],  1.5708, eps);
    ASSERT_NEAR(thetas[7],  2.0944, eps);
    ASSERT_NEAR(thetas[8], -3.14159, eps);
    ASSERT_NEAR(thetas[9],  3.1419, eps);
}

TEST(proj, none) {
    using namespace std;

    auto proj_uv = proj({2,3,1}, {3,4,-1}, NONE);

    long double eps = 1E-12;
    ASSERT_NEAR(proj_uv(0),  3.0, eps);
    ASSERT_NEAR(proj_uv(1),  4.0, eps);
    ASSERT_NEAR(proj_uv(2), -1.0, eps);
}

TEST(proj, parallel) {
    using namespace std;

    auto proj_uv = proj({2,3,1}, {3,4,-1}, PARALLEL);

    long double eps = 1E-12;
    ASSERT_NEAR(proj_uv(0), 2.4285714285714285714, eps);
    ASSERT_NEAR(proj_uv(1), 3.6428571428571428571, eps);
    ASSERT_NEAR(proj_uv(2), 1.2142857142857142857, eps);
}

TEST(proj, perpendicular) {
    using namespace std;

    auto proj_uv = proj({2,3,1}, {3,4,-1}, PERPENDICULAR);

    long double eps = 1E-12;
    ASSERT_NEAR(proj_uv(0),  0.57142857142857142857, eps);
    ASSERT_NEAR(proj_uv(1),  0.35714285714285714286, eps);
    ASSERT_NEAR(proj_uv(2), -2.2142857142857142857, eps);
}
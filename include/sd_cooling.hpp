//
// Created by L. Nagy on 24/11/2020.
//

#ifndef SD_COOLING_SD_COOLING_HPP
#define SD_COOLING_SD_COOLING_HPP

//#include <ranges>
#include <vector>
#include <algorithm>
#include <numeric>

#include "constants.hpp"
#include "basic_types.hpp"
#include "utilities.hpp"

#include "grain_population.hpp"
#include "stoner_wohlfarth.hpp"

struct GrainPopulationFraction {
    std::shared_ptr<GrainPopulation> grain_population;
    Real fraction;
};

class Assemblage {
public:

    explicit Assemblage(Real Tinit, Real eps=1E-12, ISize npolish=0): _Tinit(Tinit), _eps(eps), _npolish(npolish) {}

    void add_stoner_wohlfarth_population(Real esvd, Real elong,
                                         Real H, Real hx, Real hy, Real hz,
                                         Real ux, Real uy, Real uz,
                                         const std::string &material,
                                         Real rho0, Real rho1, Real fraction) {
        // Push back a new grain with the given parameters, along with its fraction.
        _grain_population_fractions.push_back(
                {
                    std::make_shared<StonerWohlfarthGrainPopulation>(
                        esvd, elong, H, hx, hy, hz, ux, uy, uz, material, _Tinit, Vector2D(rho0, rho1), _eps, _npolish
                    ),
                    fraction
                }
        );
    }

    void update_field(Real H) {
        std::for_each(_grain_population_fractions.begin(), _grain_population_fractions.end(),
                      [H] (GrainPopulationFraction &gp) {
                          gp.grain_population->update_field(H);
                      }
        );
    }

    void update_field(Real H, Real hx, Real hy, Real hz) {
        std::for_each(_grain_population_fractions.begin(), _grain_population_fractions.end(),
                      [H, hx, hy, hz] (GrainPopulationFraction &gp) {
                          gp.grain_population->update_field(H, hx, hy, hz);
                      }
        );
    }

    void update_temperature(Real T) {
        std::for_each(_grain_population_fractions.begin(), _grain_population_fractions.end(),
            [T] (GrainPopulationFraction &gp) {
                gp.grain_population->update_temperature(T);
            }
        );
    }

    void update_rho(Real dt) {
        std::for_each(_grain_population_fractions.begin(), _grain_population_fractions.end(),
            [dt] (GrainPopulationFraction &gp) {
                gp.grain_population->update_rho(dt);
            }
        );
    }

    void update_temperature_and_rho(Real T, Real dt) {
        std::for_each(_grain_population_fractions.begin(), _grain_population_fractions.end(),
            [T, dt] (GrainPopulationFraction &gp) {
                gp.grain_population->update_temperature(T);
                gp.grain_population->update_rho(dt);
            }
        );
    }

    void equilibriate() {
        std::for_each(_grain_population_fractions.begin(), _grain_population_fractions.end(),
            [] (GrainPopulationFraction &gp) {
                gp.grain_population->equilibriate();
            }
        );
    }

    Vector3D population_magnetization(ProjectionType ptype, bool with_ms) const {
        Vector3D result = {0.0, 0.0, 0.0};

        for (const auto &gp : _grain_population_fractions) {
            result += gp.fraction * gp.grain_population->population_magnetization(ptype, with_ms);
        }

        return result;
    }

    Vector3D equilibrium_magnetization(ProjectionType ptype, bool with_ms) const {
        Vector3D result = {0.0, 0.0, 0.0};

        for (const auto &gp : _grain_population_fractions) {
            result += gp.fraction * gp.grain_population->equilibrium_magnetization(ptype, with_ms);
        }

        return result;
    }

private:
    std::vector< GrainPopulationFraction > _grain_population_fractions;

    std::vector< std::array<Real, 9> > _data_cache;

    Real _Tinit;
    Real _eps;
    ISize _npolish;
};

#endif //SD_COOLING_SD_COOLING_HPP

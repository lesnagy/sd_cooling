//
// Created by L. Nagy on 22/10/2020.
//

#ifndef SD_COOLING_STONER_WOHLFARTH_HPP
#define SD_COOLING_STONER_WOHLFARTH_HPP

#include <cmath>

#include <algorithm>
#include <functional>
#include <tuple>
#include <iostream>
#include <iomanip>

#include <boost/multiprecision/cpp_dec_float.hpp>
#include <boost/multiprecision/eigen.hpp>

#include "constants.hpp"
#include "materials.hpp"
#include "grain_population.hpp"
#include "debug.hpp"

/**
 * This exception class is thrown if an unexpeced number (i.e. more than two) minima are found for the Stoner-Wohlfarth
 * energy functional.
 */
class MoreThanTwoMinimaException: public std::exception
{
    [[nodiscard]] const char* what() const noexcept override {
        return "More than two minima were found for the Stoner-Wohlfarth energy functional.";
    }
};

/**
 * Long axis demagnetizing factor using Dunlop & Ozdemir's convention, (Note: Dunlop & Ozdemir denote the short axis `b`
 * and the long axis `a` [pp. 90]; Cullity & Graham denote the short axis `a` and the long axis `c` [pp. 54]).
 * @param m the aspect ratio of the prolate spheroid, m = a/b.
 * @return the long axis demagnetizing factor.
 */
inline Real
Na(Real m)
{
    return (1.0 / (m*m - 1.0)) * ( (m/sqrt(m*m - 1.0)) * log(m + sqrt(m*m - 1.0)) - 1.0 );
}

/**
 * Short axis demagnetizing factor using Dunlop & Ozdemir's convention, (Note: Dunlop & Ozdemir denote the short axis
 * `b` and the long axis `a` [pp. 90]; Cullity & Graham denote the short axis `a` and the long axis `c` [pp. 54]).
 * @param m the aspect ratio of the prolate spheroid, m = a/b.
 * @return the short axis demagnetizing factor.
 */
inline Real
Nb(Real m)
{
    return 0.5 * (1.0 - Na(m));
}

/**
 * Return the C1 constant for the Stoner-Wohlfarth energy functional, this is the constant associated with the
 * demagnetizing part.
 * @param esvd equivalent spherical volume diameter [m].
 * @param elong elongation as a percentage (100% elongation is twice length to height) [%].
 * @param material the name of the material for which C1 is required.
 * @return the first S-W energy functional constant, as a function of temperature [J].
 */
std::function<Real(Real)>
C1_function(Real esvd, Real elong, const std::string & material)
{
    Real m = (100.0 + elong) / 100.0;
    Real v = (1.0 / 6.0) * pi * esvd * esvd * esvd;
    Real C = 0.5 * (Nb(m) - Na(m)) * v * mu0;
    auto Ms = saturation_magnetization_function(name_to_material(material));

    return [C, Ms] (Real T) -> Real {
        return C * Ms(T) * Ms(T);
    };
}

/**
 * Return the volume normalized C1 constant for the Stoner-Wohlfarth energy functional, this is the constant associated
 * with the demagnetizing part.
 * @param elong elongation as a percentage (100% elongation is twice length to height) [%].
 * @param material the name of the material for which C1 is required.
 * @return the first S-W energy functional constant, as a function of temperature [J].
 */
std::function<Real(Real)>
C1VN_function(Real elong, const std::string & material)
{
    Real m = (100.0 + elong) / 100.0;
    Real C = 0.5 * (Nb(m) - Na(m)) * mu0;
    auto Ms = saturation_magnetization_function(name_to_material(material));

    return [C, Ms] (Real T) -> Real {
        return C * Ms(T) * Ms(T);
    };
}

/**
 * Return the C2 constant for the Stoner-Wohlfarth energy functional, this is the constant associated with the
 * parallel component of the applied field part.
 * @param esvd equivalent spherical volume diameter [m].
 * @param H the strength of the applied external field [A/m].
 * @param phi the direction of the applied external field (relative to the axis of elongation) [rad].
 * @param material the name of the material for which C1 is required.
 * @return the second S-W energy functional constant, as a function of temperature.
 */
std::function<Real(Real)>
C2_function(Real esvd, Real H, Real phi, const std::string & material)
{
    Real v = (1.0 / 6.0) * pi * esvd * esvd * esvd;
    Real C = H * v * mu0 * cos(phi);
    auto Ms = saturation_magnetization_function(name_to_material(material));

    return [C, Ms] (Real T) -> Real {
        return C * Ms(T);
    };
}

/**
 * Return the volume normalized C2 constant for the Stoner-Wohlfarth energy functional, this is the constant associated
 * with the parallel component of the applied field part.
 * @param H the strength of the applied external field [A/m].
 * @param phi the direction of the applied external field (relative to the axis of elongation) [rad].
 * @param material the name of the material for which C1 is required.
 * @return the second S-W energy functional constant, as a function of temperature.
 */
std::function<Real(Real)>
C2VN_function(Real H, Real phi, const std::string & material)
{
    Real C = H * mu0 * cos(phi);
    auto Ms = saturation_magnetization_function(name_to_material(material));

    return [C, Ms] (Real T) -> Real {
        return C * Ms(T);
    };
}

/**
 * Return the C3 constant for the Stoner-Wohlfarth energy functional, this is the constant associated with the
 * perpendicular component of the applied field part.
 * @param esvd equivalent spherical volume diameter [m].
 * @param H the strength of the applied external field [A/m].
 * @param phi the direction of the applied external field (relative to the axis of elongation) [rad].
 * @param material the name of the material for which C1 is required.
 * @return the third S-W energy functional constant, as a function of temperature.
 */
std::function<Real(Real)>
C3_function(Real esvd, Real H, Real phi, const std::string & material)
{
    Real v = (1.0 / 6.0) * pi * esvd * esvd * esvd;
    Real C = H * v * mu0 * sin(phi);
    auto Ms = saturation_magnetization_function(name_to_material(material));

    return [C, Ms] (Real T) -> Real {
        return C * Ms(T);
    };
}

/**
 * Return the volume normalized C3 constant for the Stoner-Wohlfarth energy functional, this is the constant associated
 * with the perpendicular component of the applied field part.
 * @param H the strength of the applied external field [A/m].
 * @param phi the direction of the applied external field (relative to the axis of elongation) [rad].
 * @param material the name of the material for which C1 is required.
 * @return the third S-W energy functional constant, as a function of temperature.
 */
std::function<Real(Real)>
C3VN_function(Real H, Real phi, const std::string & material)
{
    Real C = H * mu0 * sin(phi);
    auto Ms = saturation_magnetization_function(name_to_material(material));

    return [C, Ms] (Real T) -> Real {
        return C * Ms(T);
    };
}

std::function<Complex(Real)>
CC1_function(Real elong, Real H, Real phi, const std::string & material)
{
    Real m = (100.0 + elong) / 100.0;
    Real C = (2.0 * H) / ((Nb(m) - Na(m)));

    auto Ms = saturation_magnetization_function(name_to_material(material));

    return [C, phi, Ms] (Real T) -> Complex {
        return Complex(C * cos(phi) / Ms(T), C * sin(phi) / Ms(T) );
    };
}

std::function<Complex(Real)>
CC2_function(Real elong, Real H, Real phi, const std::string & material)
{
    Real m = (100.0 + elong) / 100.0;
    Real C = (2.0 * H) / ((Nb(m) - Na(m)));

    auto Ms = saturation_magnetization_function(name_to_material(material));

    return [C, phi, Ms] (Real T) -> Complex {
        return Complex(C * cos(phi) / Ms(T), (-1.0) * C * sin(phi) / Ms(T) );
    };
}

/**
 * Return the Stoner-Wohlfarth energy functional for the given set of input parameters describing a grain.
 * @param esvd equivalent spherical volume diameter [m].
 * @param elong the elongation of the ellipsoid as a percentage [%].
 * @param H the strength of the applied field [A/m].
 * @param phi the direction of the applied field [rad].
 * @param material the material that the grain is made of.
 * @return the S-W energy functional (as a function of magnetization direction, theta and temperature in centigrade) for
 *         the grain described by the input parameters
 */
std::function<Real(Real, Real)>
stoner_wohlfarth_energy_function(Real esvd, Real elong, Real H, Real phi, const std::string & material)
{
    auto C1 = C1_function(esvd, elong, material);
    auto C2 = C2_function(esvd, H, phi, material);
    auto C3 = C3_function(esvd, H, phi, material);

    return [C1, C2, C3] (Real theta, Real T) -> Real {
        return C1(T)*sin(theta)*sin(theta) - C2(T)*cos(theta) - C3(T)*sin(theta);
    };
}

/**
 * Return the Stoner-Wohlfarth energy density functional for the given set of input parameters describing a grain.
 * @param elong the elongation of the ellipsoid as a percentage [%].
 * @param H the strength of the applied field [A/m].
 * @param phi the direction of the applied field [rad].
 * @param material the material that the grain is made of.
 * @return the S-W energy density functional (as a function of magnetization direction, theta and temperature in
 *         centigrade) for the grain described by the input parameters
 */
std::function<Real(Real, Real)>
stoner_wohlfarth_energy_density_function(Real elong, Real H, Real phi, const std::string & material)
{
    auto C1VN = C1VN_function(elong, material);
    auto C2VN = C2VN_function(H, phi, material);
    auto C3VN = C3VN_function(H, phi, material);

    return [C1VN, C2VN, C3VN] (Real theta, Real T) -> Real {
        return C1VN(T)*sin(theta)*sin(theta) - C2VN(T)*cos(theta) - C3VN(T)*sin(theta);
    };
}

/**
 * Return the first derivative of the Stoner-Wohlfarth energy functional for the given set of input parameters
 * describing a grain.
 * @param esvd equivalent spherical volume diameter [m].
 * @param elong the elongation of the ellipsoid as a percentage [%].
 * @param H the strength of the applied field [A/m].
 * @param phi the direction of the applied field [rad].
 * @param material the material that the grain is made of.
 * @return the first derivative S-W energy functional (as a function of magnetization direction, theta and temperature
 *         in centigrade) for the grain described by the input parameters
 */
std::function<Real(Real, Real)>
d_stoner_wohlfarth_energy_function(Real esvd, Real elong, Real H, Real phi, const std::string & material)
{
    auto C1 = C1_function(esvd, elong, material);
    auto C2 = C2_function(esvd, H, phi, material);
    auto C3 = C3_function(esvd, H, phi, material);

    return [C1, C2, C3] (Real theta, Real T) -> Real {
        return 2.0*C1(T)*sin(theta)*cos(theta) + C2(T)*sin(theta) - C3(T)*cos(theta);
    };
}

/**
 * Return the first derivative of the Stoner-Wohlfarth energy-density functional for the given set of input parameters
 * describing a grain.
 * @param elong the elongation of the ellipsoid as a percentage [%].
 * @param H the strength of the applied field [A/m].
 * @param phi the direction of the applied field [rad].
 * @param material the material that the grain is made of.
 * @return the first derivative S-W energy-density functional (as a function of magnetization direction, theta and
 *         temperature in centigrade) for the grain described by the input parameters
 */
std::function<Real(Real, Real)>
d_stoner_wohlfarth_energy_density_function(Real elong, Real H, Real phi, const std::string & material)
{
    auto C1VN = C1VN_function(elong, material);
    auto C2VN = C2VN_function(H, phi, material);
    auto C3VN = C3VN_function(H, phi, material);

    return [C1VN, C2VN, C3VN] (Real theta, Real T) -> Real {
        return 2.0*C1VN(T)*sin(theta)*cos(theta) + C2VN(T)*sin(theta) - C3VN(T)*cos(theta);
    };
}

/**
 * Return the second derivative of the Stoner-Wohlfarth energy functional for the given set of input parameters
 * describing a grain.
 * @param esvd equivalent spherical volume diameter [m].
 * @param elong the elongation of the ellipsoid as a percentage [%].
 * @param H the strength of the applied field [A/m].
 * @param phi the direction of the applied field [rad].
 * @param material the material that the grain is made of.
 * @return the second derivative S-W energy functional (as a function of magnetization direction, theta and temperature
 *         in centigrade) for the grain described by the input parameters
 */
std::function<Real(Real, Real)>
dd_stoner_wohlfarth_energy_function(Real esvd, Real elong, Real H, Real phi, const std::string & material)
{
    auto C1 = C1_function(esvd, elong, material);
    auto C2 = C2_function(esvd, H, phi, material);
    auto C3 = C3_function(esvd, H, phi, material);

    return [C1, C2, C3] (Real theta, Real T) -> Real {
        return 2.0*C1(T)*cos(theta)*cos(theta) - 2.0*C1(T)*sin(theta)*sin(theta) + C2(T)*cos(theta) + C3(T)*sin(theta);
    };
}

/**
 * Return the second derivative of the Stoner-Wohlfarth energy density functional for the given set of input parameters
 * describing a grain.
 * @param elong the elongation of the ellipsoid as a percentage [%].
 * @param H the strength of the applied field [A/m].
 * @param phi the direction of the applied field [rad].
 * @param material the material that the grain is made of.
 * @return the second derivative S-W energy density functional (as a function of magnetization direction, theta and
 *         temperature in centigrade) for the grain described by the input parameters
 */
std::function<Real(Real, Real)>
dd_stoner_wohlfarth_energy_density_function(Real elong, Real H, Real phi, const std::string & material)
{
    auto C1VN = C1VN_function(elong, material);
    auto C2VN = C2VN_function(H, phi, material);
    auto C3VN = C3VN_function(H, phi, material);

    return [C1VN, C2VN, C3VN] (Real theta, Real T) -> Real {
        return 2.0*C1VN(T)*cos(theta)*cos(theta) - 2.0*C1VN(T)*sin(theta)*sin(theta) + C2VN(T)*cos(theta) + C3VN(T)*sin(theta);
    };
}

/**
 * This exception class is thrown if a grain population is invalid.
 */
class InvalidGrainPopulation: public std::exception
{
public:
    InvalidGrainPopulation() {}

    InvalidGrainPopulation(const std::string& msg): _msg(msg) {}

    [[nodiscard]] const char* what() const noexcept override {
        return _msg.c_str();
    }

private:
    std::string _msg;
};



/**
 * Simulate a uniaxial grain population.
 */
class StonerWohlfarthGrainPopulation: public GrainPopulation
{
public:
    StonerWohlfarthGrainPopulation(Real esvd,
                                   Real elong,
                                   Real H,
                                   Real hx, Real hy, Real hz,
                                   Real ux, Real uy, Real uz,
                                   const std::string &str_material,
                                   Real Tinit,
                                   Vector2D rho,
                                   Real eps = 1E-12,
                                   ISize n_polish = 0) :
            _eps(eps),
            _n_polish(n_polish) {

        // Set the volume.
        _set_volume(esvd);

        // Set the elongation.
        _set_elongation(elong);

        // Set the orientation.
        _set_orientation(ux, uy, uz);

        // Set the material.
        _set_material(str_material);

        // Set the field.
        _set_field(H, hx, hy, hz);

        // Set temperature dependent constant functions.
        _set_cc1_cc2();

        // Set temperature depenent Stoner-Wohlfarth functions and their 1st/2nd derivatives.
        _set_sw();

        // Set the initial temperature.
        _set_Tinit(Tinit);

        // Set the population proportions.
        _set_rho(rho);

        // Set the current temperature to the inital temperature
        _initialize_model();

        // Validate the object.
        _check_object();
    }

    virtual void update_temperature(Real T) {
        // Update the current temperature.
        _T = T;
        _compute_lems_and_energy_barriers();
    }

    virtual void equilibriate() {
        // Equilibriate the model, i.e. assume that the model sits at _T for an infinite amount of time and update
        // rho with those values.

        // Compute LEMs and energy barriers for current T_.
        _compute_lems_and_energy_barriers();

        // Retrive a possible equilibrium rho.
        auto rho_eq = _compute_equilibrium_rho();


        if (isfinite(rho_eq(0)) && isfinite(rho_eq(1))) {
            // update the population with the equilibrium rho, otherwise we'll just assume that the energy barriers are too
            // high and that the current population rho distribution is 'frozen in'.
            _rho = rho_eq;
            _rho_eq = rho_eq;
        } else {
            // Use the existing equilibrium rho distribution for the population rho.
            _rho = _rho_eq;
        }
    }

    void update_field(Real H) {
        update_field(H, _hx, _hy, _hz);
    }

    void update_field(Real H, Real hx, Real hy, Real hz) {
        // Set the field to the new values.
        _set_field(H, hx, hy, hz);

        // Set temperature dependent constant functions.
        _set_cc1_cc2();

        // Set temperature depenent Stoner-Wohlfarth functions and their 1st/2nd derivatives.
        _set_sw();

        // Compute the LEMS and energy barriers associated with the new values.
        _compute_lems_and_energy_barriers();
    }

    /**
     * Update the population partition as a function of time spent at the current population temperature.
     * @param dt
     */
    void update_rho(Real dt) {
        // Update the population magnetization.
        auto P = _compute_probability_tranistion_matrix(dt);
        _rho = P * _rho;

        // Update the equilibrium magnetization.
        auto rho_eq = _compute_equilibrium_rho();
        if (isfinite(rho_eq(0)) && isfinite(rho_eq(1))) {
            // Only update the equilibrium rho if both values in the new equilibrium rho are smaller than infinity,
            // otherwise assume that the energy barriers are too high and that the last distribution of equilibrium
            // states is valid.
            _rho_eq = rho_eq;
        }
    }

    /**
     * Compute the population magnetization.
     * @param ptype the projection to use, NONE just returns the raw vector, PARALLEL is the magnetization component
     *              parallel to the applied field direction, and PERPENDICULAR is the component perpendicular to the
     *              applied field direction (assuming a right handed rule). By default this is NONE.
     * @param with_ms include the temperature depenent spontaneous magnetization. By default this is false.
     * @return a three dimensional magnetization vector.
     */
    Vector3D population_magnetization(ProjectionType ptype=NONE, bool with_ms=false) const {
        // Sanity check.
        assert(_theta_minima.size() == 2);

        auto rax = _u.cross(_h).normalized();

        Vector3D m = {0.0, 0.0, 0.0};
        auto R1 = rotation_matrix(rax, _theta_minima[0]);
        auto m1 = R1*_u;

        auto R2 = rotation_matrix(rax, _theta_minima[1]);
        auto m2 = R2*_u;

        m = _rho[0]*m1 + _rho[1]*m2;

        // Do the projection.
        m = proj(_h, m, ptype);

        // Multiply by Ms.
        if (with_ms) {
            return _ms(_T) * m;
        } else {
            return m;
        }
    }

    /**
     * Compute the equilibrium magnetization (i.e. t -> infity).
     * @param ptype the projection to use, NONE just returns the raw vector, PARALLEL is the magnetization component
     *              parallel to the applied field direction, and PERPENDICULAR is the component perpendicular to the
     *              applied field direction (assuming a right handed rule). By default this is NONE.
     * @param with_ms include the temperature depenent spontaneous magnetization. By default this is false.
     * @return a three dimensional magnetization vector.
     */
    Vector3D equilibrium_magnetization(ProjectionType ptype=NONE, bool with_ms=false) const {
        // Sanity check.
        assert(_theta_minima.size() == 2);

        auto rax = _u.cross(_h).normalized();

        Vector3D m = {0.0, 0.0, 0.0};
        auto R1 = rotation_matrix(rax, _theta_minima[0]);
        auto m1 = R1*_u;

        auto R2 = rotation_matrix(rax, _theta_minima[1]);
        auto m2 = R2*_u;

        m = _rho_eq[0]*m1 + _rho_eq[1]*m2;

        // Do the projection.
        m = proj(_h, m, ptype);

        // Multiply by Ms.
        if (with_ms) {
            return _ms(_T) * m;
        } else {
            return m;
        }
    }

    /**
     * Retrieve the vector containing the fraction of grains in one state and the fraction of grains in the other.
     * @return a 2D vector with the proportion of grains in state one and the proportion of grains in state two.
     */
    Vector2D get_rho() {
        return _rho;
    }

private:
    // StonerWohlfarthGrainPopulation constants.
    Real _esvd;
    Real _elong;
    Real _H;
    Real _hx;
    Real _hy;
    Real _hz;
    Real _ux;
    Real _uy;
    Real _uz;
    std::string _str_material;
    Real _Tinit;
    Vector2D _rho;
    Vector2D _rho_eq;
    Real _eps;
    ISize _n_polish;

    // Derived constant values.
    Real _v;

    // Derived values

    // The angle at which the applied field is directed.
    Real _phi;

    // The material enumeration.
    Material _material;

    // A unit vector pointing in the direction of the applied field.
    Vector3D _h;

    // A unit vector pointing in the direction of the grain's principal axis.
    Vector3D _u;

    // Material dependent functions.
    std::function<Complex(Real)> _CC1;
    std::function<Complex(Real)> _CC2;

    std::function<Real(Real, Real)> _sw;
    std::function<Real(Real, Real)> _dsw;
    std::function<Real(Real, Real)> _ddsw;

    std::function<Real(Real)> _ms;

    // The temperature.
    Real _T;

    // Theta values that correspond to energy minima.
    std::vector<Real> _theta_minima;

    // Theta values that correspond to energy maxima.
    std::vector<Real> _theta_maxima;

    // Theta values that correspond to energy inflection points.
    std::vector<Real> _theta_inflct;

    // Energy barriers.
    std::vector< std::vector<Real> > _energy_barriers;

    // Private functions.

    void _check_object() {
        // Epsilon must be non-zero
        if (_eps < 0.0) {
            throw InvalidGrainPopulation("Epsilon is non-zero.");
        }

        // Equivalent sp
        // herical volume diameter (ESVD) must be non-zero.
        if (_esvd < _eps) {
            throw InvalidGrainPopulation("ESVD was too small or negative.");
        }

        // Elongation must be non-zero.
        if (_elong < _eps) {
            throw InvalidGrainPopulation("Elongation was too small or negative");
        }

        // Population must sum to 1.0.
        // TODO: implement this.
    }

    /**
     * Set the material name string and the material enumeration.
     * @param material the material name.
     */
    void _set_material(const std::string &material) {
        // Set the material string.
        _str_material = material;

        // Set the material enumeration.
        _material = name_to_material(_str_material);

        // Set saturation magnetization function.
        _ms = saturation_magnetization_function(name_to_material(_str_material));
    }

    /**
     * Set the grain populations volume by calculating the volume of a sphere from the equivalent spherical volume
     * diameter (esvd).
     * @param esvd equivalend spherical volume diameter (in meter).
     */
    void _set_volume(Real esvd) {
        _esvd = esvd;
        _v = (pi / 6.0) * _esvd * _esvd * _esvd;
    }

    /**
     * Set the grain population's elongation.
     * @param elong the elongation.
     */
    void _set_elongation(Real elong) {
        _elong = elong;
    }

    /**
     * Set the grain population's orientation.
     * @param ux the x component of the orientation vector.
     * @param uy the y component of the orientation vector.
     * @param uz the z component of the orientation vector.
     */
    void _set_orientation(Real ux, Real uy, Real uz) {
        // Record the values of u.
        _ux = ux;
        _uy = uy;
        _uz = uz;

        // Compute a normalized version of u.
        _u(0) = _ux;
        _u(1) = _uy;
        _u(2) = _uz;
        _u.normalize();
    }

    /**
     * Set the applied field associated variables.
     * @param H the applied field strength (A/m).
     * @param hx the x component of the applied field direction vector.
     * @param hy the y component of the applied field direction vector.
     * @param hz the z component of the applied field direction vector.
     */
    void _set_field(Real H, Real hx, Real hy, Real hz) {
        // Update applied field strength.
        _H = H;

        // Record applied field direction components.
        _hx = hx;
        _hy = hy;
        _hz = hz;

        // Compute normalized version of h.
        _h(0) = _hx;
        _h(1) = _hy;
        _h(2) = _hz;
        _h.normalize();

        // Compute planar angle of applied field.
        _phi = acos(_h.dot(_u));
    }

    /**
     * Set the temperature dependent CC1 and CC2 constants.
     */
    void _set_cc1_cc2() {
        _CC1 = CC1_function(_elong, _H, _phi, _str_material);
        _CC2 = CC2_function(_elong, _H, _phi, _str_material);
    }

    /**
     * Set the Stoner-Wohlfarth energy functionals.
     */
    void _set_sw() {
        _sw = stoner_wohlfarth_energy_density_function(_elong, _H, _phi, _str_material);
        _dsw = d_stoner_wohlfarth_energy_density_function(_elong, _H, _phi, _str_material);
        _ddsw = dd_stoner_wohlfarth_energy_density_function(_elong, _H, _phi, _str_material);
    }

    /**
     * Set the model's initial temperature.
     * @param Tinit the model's initial temperature.
     */
    void _set_Tinit(Real Tinit) {
        _Tinit = Tinit;
    }

    /**
     * Set the model's rho - distribution of states.
     * @param rho the rho vector.
     */
    void _set_rho(Vector2D rho) {
        // Normalize rho so that each entry sums to 1.
        auto nl = abs(rho(0) + rho(1));
        _rho(0) = abs(rho(0)) / nl;
        _rho(1) = abs(rho(1)) / nl;
    }

    /**
     * Initialize the model to the initial temperature and compute the LEMs and the energy barriers.
     */
    void _initialize_model() {
        // Set the current time to _Tinit.
        _T = _Tinit;

        // Compute the LEM states and energy barriers.
        _compute_lems_and_energy_barriers();
    }

    /**
     * Polish a root using `niter` number of iterations of Newton-Raphson root finder.
     * @param theta0 the theta value to polish.
     * @param niter the number of iterations (should be small).
     * @return a polished version of the input `theta`.
     */
    Real _newton_raphson_polisher(Real theta0, ISize niter) {
        Real th = theta0;
        for (Index i = 0; i < niter; ++i) {
            auto nrcorrect = _dsw(th, _T) / _ddsw(th, _T);
            th = th - nrcorrect;
        }
        return th;
    }

    /**
     * Compute the LEM states (corresponding to angles) using the Stoner-Wolhfath model.
     */
    void _compute_lems_and_energy_barriers() {
        using namespace std;

        // Clear the theta matrices which correspond to Stoner-Wohlfarth energy functional extrema.
        _theta_minima.clear();
        _theta_maxima.clear();
        _theta_inflct.clear();

        // Compute the Hessendorf matrix.
        ComplexMatrix4x4 H;
        H(0, 0) = Complex(-1, 0) * _CC1(_T);
        H(0, 1) = Complex(0.0, 0.0);
        H(0, 2) = _CC2(_T);
        H(0, 3) = Complex(1.0, 0.0);

        H(1, 0) = Complex(1.0, 0.0);
        H(1, 1) = Complex(0.0, 0.0);
        H(1, 2) = Complex(0.0, 0.0);
        H(1, 3) = Complex(0.0, 0.0);

        H(2, 0) = Complex(0.0, 0.0);
        H(2, 1) = Complex(1.0, 0.0);
        H(2, 2) = Complex(0.0, 0.0);
        H(2, 3) = Complex(0.0, 0.0);

        H(3, 0) = Complex(0.0, 0.0);
        H(3, 1) = Complex(0.0, 0.0);
        H(3, 2) = Complex(1.0, 0.0);
        H(3, 3) = Complex(0.0, 0.0);

        // Compute the eigenvalues of the Hessendorf matrix.
        auto eigen_values = H.eigenvalues();

        for (auto i = 0; i < eigen_values.size(); ++i) {

            // For each eigenvalue of H matrix, apply the transform theta = i * log(x).
            auto theta = Complex(0.0, 1.0) * log(eigen_values(i));

            if (abs(theta.imag()) < _eps) {
                // If the imaginary part is small enough (i.e. theta is real) then continue processing.
                auto theta_real = theta.real();

                // Polish the theta if required.
                if (_n_polish > 0) {
                    theta_real = _newton_raphson_polisher(theta_real, _n_polish);
                }

                // Check if `theta_real` is a minimum/maximum or inflection.
                if (_ddsw(theta_real, _T) < 0.0) {
                    // `theta_real` must be a maximum.
                    _theta_maxima.push_back(theta_real);
                } else if (_ddsw(theta_real, _T) > 0.0) {
                    // `theta_real` must be a minimum.
                    _theta_minima.push_back(theta_real);
                } else {
                    // `theta_real` must be an inflection point so we add it to the `theta_inflct` list.
                    _theta_inflct.push_back(theta_real);
                }
            }
        }

        // Sort theta minima values by nearness to the applied field direction.
        nearness_sort(_theta_minima, _phi);

        _energy_barriers.clear();
        for (Index i = 0; i < _theta_minima.size(); ++i) {
            // For each of the thetas corresponding to energy minima.
            auto theta_min = _theta_minima[i];

            // Energy barriers from the ith LEM to other LEMs are stored here.
            vector<Real> ith_barriers;

            // Begin calculating the smallest barrier value between theta_min and the one other LEM state,
            // recall that this is a uniaxial model.
            Real smallest_barrier = 1E20;
            for (auto theta_max : _theta_maxima) {
                // For each of the thetas corresponding to energy maxima, compute an energy barrier.
                auto Eb = _v * (_sw(theta_max, _T) - _sw(theta_min, _T));
                if (Eb < smallest_barrier) {
                    // If the energy barrier is smaller than the current value for the energy barrier then use the
                    // smaller value as the energy barrier corresponding with the `theta_min` value.
                    smallest_barrier = Eb;
                }
            }

            // Add the smallest barrer we could find to the barriers associated with the ith LEM. Note: for uniaxial-SD,
            // there is only one entry.
            ith_barriers.push_back(smallest_barrier);

            // Add it to the energy barriers.
            _energy_barriers.push_back(ith_barriers);
        }

        // If there was only one minimum found, then repeat the barriers.
        if (_theta_minima.size() == 1) {
            _energy_barriers.push_back(_energy_barriers[0]);
        }

        // If there was only on minimum found, then repeat the minima. What does this do? If there is only
        // one minima found, then by repeating the same minima, it means that when compute the weighted average
        // we find that the magnetization is just whatever is associated with m.
        if (_theta_minima.size() == 1) {
            _theta_minima.push_back(_theta_minima[0]);
        }

    }

    RealMatrix2x2
    _compute_probability_tranistion_matrix(Real t) {
        using namespace std;

		using MPReal= boost::multiprecision::number<
			boost::multiprecision::cpp_dec_float<500>
		>;

		using MPMatrix2x2 = Eigen::Matrix<MPReal, 2, 2>;

		using Solver = Eigen::EigenSolver<MPMatrix2x2>;

        // Construct the probability transition matrix.
        RealMatrix2x2 Q;

        // state 1 to state 0 transition (negative)
        Q(0, 0) = (-1.0 / tau0) * exp(-1.0 * _energy_barriers[1][0] / ((_T + 273.15) * kb)) * t;

        // state 0 to state 1 transition
        Q(0, 1) = (1.0 / tau0) * exp(-1.0 * _energy_barriers[0][0] / ((_T + 273.15) * kb)) * t;

        // state 1 to state 0 transition
        Q(1, 0) = (1.0 / tau0) * exp(-1.0 * _energy_barriers[1][0] / ((_T + 273.15) * kb)) * t;

        // state 0 to state 1 transition (negative)
        Q(1, 1) = (-1.0 / tau0) * exp(-1.0 * _energy_barriers[0][0] / ((_T + 273.15) * kb)) * t;

        // Calculate matrix exponential using matrix diagonalization.
        Solver solver;
        solver.compute(Q);

        auto eigen_values = solver.eigenvalues();
        auto eigen_vectors = solver.eigenvectors();

        MPMatrix2x2 expD;
        expD(0,0) = boost::multiprecision::exp(eigen_values(0).real());
        expD(1,1) = boost::multiprecision::exp(eigen_values(1).real());

        MPMatrix2x2 P;
        P(0,0) = eigen_vectors(0,0).real();
        P(0,1) = eigen_vectors(0,1).real();
		P(1,0) = eigen_vectors(1,0).real();
		P(1,1) = eigen_vectors(1,1).real();

		MPMatrix2x2 invP = P.inverse();

		MPMatrix2x2 expQ = P * expD * invP;

		RealMatrix2x2 R;
		R(0,0) = (Real)expQ(0,0);
		R(0,1) = (Real)expQ(0,1);
		R(1,0) = (Real)expQ(1,0);
		R(1,1) = (Real)expQ(1,1);

		return R;
    }

    /**
     * Compute candidate equilibrium rho values based on the energy barriers.
     */
    Vector2D
    _compute_equilibrium_rho() {
	  using MPReal= boost::multiprecision::number<
	      boost::multiprecision::cpp_dec_float<500>
	  >;

        // Sanity check to make shure that _theta_minimia consists of two energy minima.
        assert(_theta_minima.size() == 2);

        MPReal Eb12 = _energy_barriers[0][0];
        MPReal Eb21 = _energy_barriers[1][0];


        // According to equilibrium rho.
        MPReal c12 = boost::multiprecision::exp(-1.0 * Eb12 / (kb * (273.15 + _T)));
		MPReal c21 = boost::multiprecision::exp(-1.0 * Eb21 / (kb * (273.15 + _T)));

		MPReal rho0 = c12 / (c12 + c21);
		MPReal rho1 = c21 / (c12 + c21);

        //std::cout << "_T = " << std::fixed << std::setw(20) << std::setprecision(15) << _T << std::endl;

        /*
        Real Tbefore = 122.679725125841;
        Real Tafter = 122.796967063767;
        if ((Tbefore  < _T) && (_T < Tafter)) {
            std::cout << "DEBUG: "
                      << "_T   = " << std::scientific << std::setw(20) << std::setprecision(15)  << _T     << ", "
                      << "Eb12  = " << std::scientific << std::setw(20) << std::setprecision(15) << Eb12   << ", "
                      << "Eb21  = " << std::scientific << std::setw(20) << std::setprecision(15) << Eb21   << ", "
                      << "a12   = " << std::scientific << std::setw(20) << std::setprecision(15) << a12    << ", "
                      << "a21   = " << std::scientific << std::setw(20) << std::setprecision(15) << a21    << ", "
                      << "inv_b = " << std::scientific << std::setw(20) << std::setprecision(15) << inv_b  << ", "
                      << "c12   = " << std::scientific << std::setw(20) << std::setprecision(15) << c12    << ", "
                      << "c21   = " << std::scientific << std::setw(20) << std::setprecision(15) << c21    << ", "
                      << "m[0] = " << std::scientific << std::setw(20) << std::setprecision(15)  << m(0)   << ", "
                      << "m[1] = " << std::scientific << std::setw(20) << std::setprecision(15)  << m(1)   << ", "
                      << "m[2] = " << std::scientific << std::setw(20) << std::setprecision(15)  << m(2)
                      << std::endl;
        }
        */

        return Vector2D(rho0, rho1);
    }
};

#endif //SD_COOLING_STONER_WOHLFARTH_HPP

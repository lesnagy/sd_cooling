//
// Created by L. Nagy on 21/10/2020.
//

#ifndef SD_COOLING_CODE_TIMING_HPP
#define SD_COOLING_CODE_TIMING_HPP

#ifdef ENABLE_TIMING

#include <iostream>
#include <chrono>

#define TIMING_START(s) auto __ ## s ## _start = std::chrono::steady_clock::now();
#define TIMING_STOP(s)  auto __ ## s ## _end = std::chrono::steady_clock::now(); \
                        std::chrono::duration<double> __ ## s ## _elapsed = __ ## s ## _end - __ ## s ## _start; \
                        double __ ## s ## _elapsed_seconds = __ ## s ## _elapsed.count();
#define TIMING_INFO(s)  {\
                            std::cerr << "[ TIMING   ] " << "Section '" << #s << "' took: " << __ ## s ## _elapsed_seconds << "s" << std::endl;\
                        }

#else

#define TIMING_START(s) do {} while(0);
#define TIMING_STOP(s) do {} while(0);
#define TIMING_INFO(s) do {} while(0);

#endif // ENABLE_TIMING

#endif //SD_COOLING_CODE_TIMING_HPP

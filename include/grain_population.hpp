//
// Created by L. Nagy on 25/11/2020.
//

#ifndef SD_COOLING_GRAIN_POPULATION_HPP
#define SD_COOLING_GRAIN_POPULATION_HPP

#include "basic_types.hpp"
#include "utilities.hpp"

class GrainPopulation {
public:
    virtual void update_temperature(Real T) = 0;
    virtual void update_rho(Real t) = 0;
    virtual void update_field(Real H) = 0;
    virtual void update_field(Real H, Real hx, Real hy, Real hz) = 0;
    virtual void equilibriate() = 0;

    virtual Vector3D population_magnetization(ProjectionType ptype, bool with_ms) const = 0;
    virtual Vector3D equilibrium_magnetization(ProjectionType ptype, bool with_ms) const = 0;
};

#endif //SD_COOLING_GRAIN_POPULATION_HPP

//
// Created by L. Nagy on 26/10/2020.
//

#ifndef SD_COOLING_CONSTANTS_HPP
#define SD_COOLING_CONSTANTS_HPP

#include <cmath>

#include "basic_types.hpp"

// Pi
const Real pi = acos(-1.0);

// Permeability of free space
const Real mu0 =  4.0 * pi * 1E-7; // ( m kg ) / ( s^2 A^2 )

// Boltzmann's constant
const Real kb  =  1.38064852E-23;      // ( m^2 kg ) / ( s K )

// Switching frequency.
const Real tau0 = 1E-9; // (s), note: see Dunlop & Ozdemir (2001), pp. 202 (eqn. 8.3)

#endif //SD_COOLING_CONSTANTS_HPP

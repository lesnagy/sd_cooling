//
// Created by L. Nagy on 10/11/2020.
//

#ifndef SD_COOLING_DEBUG_HPP
#define SD_COOLING_DEBUG_HPP

#ifdef ENABLE_DEBUG_MESSAGES

#if ENABLE_DEBUG_MESSAGES == 1

#include <iostream>
#include <iomanip>
#include <vector>

#include "basic_types.hpp"

#define DEBUG_MESSAGE(msg) std::cout << msg << std::endl;

#define DEBUG_STD_VECTOR(vec, message) {                        \
    size_t counter = 0;                                     \
    std::cout << "DEBUG: printing variable " << #vec << ": " << message << std::endl;                                  \
    for (auto itr = vec.begin(); itr != vec.end(); ++itr) {     \
        std::cout << "DEBUG: ";                                                        \
        std::cout << std::fixed << std::setw(10) << counter << ") ";      \
        std::cout << std::scientific << std::setprecision(15) << std::setw(20) << *itr << std::endl;                        \
        counter++;                                                            \
    }                                                       \
}

#define DEBUG_STD_VECTOR_TABLE(vec, message) {                                                                         \
    size_t counter = 0;                                                                                                \
    std::cout << "DEBUG: printing variable " << #vec << ": " << message << std::endl;                                  \
    for (auto itr = vec.begin(); itr != vec.end(); ++itr) {                                                            \
        std::cout << "DEBUG: ";                                                                                        \
        std::cout << std::fixed << std::setw(10) << counter << ") ";                                                    \
        for (auto it = itr->begin(); it != itr->end(); ++it) {                                                         \
            std::cout << std::scientific  << std::setprecision(15) << std::setw(20) << *it << " ";        \
        }                                                                                                              \
        std::cout << std::endl;                                                                                        \
        counter++;                                                                                                     \
    }                                                                                                                  \
}

#define DEBUG_EIGEN_VECTOR(vec, message) {                                                                             \
    std::cout << "DEBUG: printing variable " << #vec << ": " << message << std::endl;                                  \
    for (size_t i = 0; i < vec.rows(); ++i) {                                                                          \
        std::cout << "DEBUG: [" << std::fixed << std::setw(20) << std::setprecision(15) << vec(i) << " ]" << std::endl;\
    }                                                                                                                  \
}

#define DEBUG_EIGEN_MATRIX(vec, message) {                                                                             \
    std::cout << "DEBUG: printing variable " << #vec << ": " << message << std::endl;                                  \
    for (size_t i = 0; i < vec.rows(); ++i) {                                                                          \
                  std::cout << "DEBUG: [";                                                                             \
        for (size_t j = 0; j < vec.cols(); ++j) {                                                                      \
        std::cout <<    std::fixed << std::setw(20) << std::setprecision(15) << vec(i,j) << " ";                       \
        }                                                                                                              \
    std::cout << "]" << std::endl;                                                                                     \
    }                                                                                                                  \
}


#else

#define DEBUG_MESSAGE(msg) do {} while (0);
#define DEBUG_STD_VECTOR(vec, message) do {} while (0);
#define DEBUG_STD_VECTOR_TABLE(vec, message) do {} while (0);
#define DEBUG_EIGEN_VECTOR(vec, message) do {} while (0);
#define DEBUG_EIGEN_MATRIX(vec, message) do {} while (0);

#endif // DEBUG_MESSAGES == 1

#else

#define DEBUG_MESSAGE(msg) do {} while (0);
#define DEBUG_STD_VECTOR(vec, message) do {} while (0);
#define DEBUG_STD_VECTOR_TABLE(vec, message) do {} while (0);
#define DEBUG_EIGEN_VECTOR(vec, message) do {} while (0);
#define DEBUG_EIGEN_MATRIX(vec, message) do {} while (0);

#endif // ENABLE_DEBUG_MESSAGES

#endif //SD_COOLING_DEBUG_HPP

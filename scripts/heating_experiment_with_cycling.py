#!python3

import sd_cooling


def command_line_parser():
    r"""
    Create a command line parser.
    :return: the comand line parser.,
    """
    parser = ArgumentParser()

    parser.add_argument("size", type=float, help="the size of the grain")
    parser.add_argument("elong", type=float, help="the percentage elongation")
    parser.add_argument("ndirs", type=int, help="the number of directions")
    parser.add_argument("Tamb", type=float, help="the ambient temperature")
    parser.add_argument("T0", type=float, help="the initial temperature")
    parser.add_argument("T1", type=float,
                        help="some temperature after heating to some time 6s "
                             "to 6E15s")
    parser.add_argument("--real-run", action="store_true",
                        help="actually run the models")

    return parser

#
# hello.py
# Tests the greeter - which is a very simple program that greets the
# user and gives some (possibly) useful information.
#

import sd_cooling
import greeter.util  # Implicitly imports greeter/__init__.py as required.

greeter.speak()
print()
greeter.python_speak()

a = 1
b = 2
c = greeter.util.add(a, b)
print()
print("The sum of {} and {} is {}!".format(a, b, c))
print()
print(sd_cooling.version_info())

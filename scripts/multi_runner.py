import os

import multiprocessing as mp

import time

from subprocess import Popen, PIPE

BASE_DIR = r"/geos/d22/shared/MicroMag/lnagy2/data/sd-cooling"
SEPARATOR = "/"

commands = [
    {
        "size": 40, "elong": 50, "dirs": 30, "Tabs": 15, "T0": 579.999,
        "T1": 15.15
    },
    {
        "size": 40, "elong": 100, "dirs": 30, "Tabs": 15, "T0": 579.999,
        "T1": 15.15
    },
    {
        "size": 40, "elong": 150, "dirs": 30, "Tabs": 15, "T0": 579.999,
        "T1": 15.15
    },
    {
        "size": 60, "elong": 50, "dirs": 30, "Tabs": 15, "T0": 579.999,
        "T1": 15.15
    },
    {
        "size": 60, "elong": 100, "dirs": 30, "Tabs": 15, "T0": 579.999,
        "T1": 15.15
    },
    {
        "size": 60, "elong": 150, "dirs": 30, "Tabs": 15, "T0": 579.999,
        "T1": 15.15
    },
    {
        "size": 80, "elong": 50, "dirs": 30, "Tabs": 15, "T0": 579.999,
        "T1": 15.15
    },
    {
        "size": 80, "elong": 100, "dirs": 30, "Tabs": 15, "T0": 579.999,
        "T1": 15.15
    },
    {
        "size": 80, "elong": 150, "dirs": 30, "Tabs": 15, "T0": 579.999,
        "T1": 15.15
    },
    {
        "size": 100, "elong": 50, "dirs": 30, "Tabs": 15, "T0": 579.999,
        "T1": 15.15
    },

    {
        "size": 100, "elong": 100, "dirs": 30, "Tabs": 15, "T0": 579.999,
        "T1": 15.15
    },
    {
        "size": 100, "elong": 150, "dirs": 30, "Tabs": 15, "T0": 579.999,
        "T1": 15.15
    }
]


def thread(id, job_queue):
    while not job_queue.empty():
        cmd = job_queue.get()

        proc = Popen(cmd, shell=True)

        stdout, stderr = proc.communicate()

        print("processor {}:\n".format(id, stdout))
    print("{} ended".format(id))


if __name__ == "__main__":
    # No. of threads.
    nthreads = 8

    # Create a (blocking) queue.
    job_queue = mp.Queue()

    # Push commands on to the queue.
    for command in commands:

        data_dir = "{base_dir:}{separator:}{size:}nm{separator:}{elong:}pc".format(
            base_dir=BASE_DIR,
            separator=SEPARATOR,
            size=command["size"],
            elong=command["elong"]
        )

        print(data_dir)
        if not os.path.isdir(data_dir):
            os.makedirs(data_dir)

        shell_command = "python monodispertion_rates.py {size:} {elong:} {dirs:} {Tabs:} {T0:} {T1:} {data_dir:} --real-run".format(
            size=command["size"],
            elong=command["elong"],
            dirs=command["dirs"],
            Tabs=command["Tabs"],
            T0=command["T0"],
            T1=command["T1"],
            data_dir=data_dir
        )
        job_queue.put(shell_command)

    # Create `nthreads` threads.
    procs = [
        mp.Process(
            target=thread,
            args=(i, job_queue)
        )
        for i in range(nthreads)
    ]

    # Start the threads.
    for p in procs:
        p.start()

    # Wait for threads to terminate.
    for p in procs:
        p.join()

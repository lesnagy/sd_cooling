#!python3

import sys

from argparse import ArgumentParser

import math
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import progressbar
import scipy.stats as ss
import sd_cooling


def d_newtonian_temperature_function_dt(Tamb, T0, T1, t1):
    k = (1.0/t1) * math.log((T0 - Tamb)/(T1 - Tamb))
    def dT_dt(t):
        return -1.0 * k * (T0-Tamb) * math.exp(-1.0*k*t)
    return dT_dt


def command_line_parser():

    parser = ArgumentParser()

    parser.add_argument("nsize", type=int, help="the number of sizes")
    parser.add_argument("nelong", type=int, help="the number of elongations")
    parser.add_argument("ndirs", type=int, help="the number of directions")
    parser.add_argument("--real-run", action="store_true", help="actually run the models")
    parser.add_argument("--cooling-info-only", action="store_true", help="print cooling run arrays only")
    return parser


def fibonacci_sphere(n=1):

    points = []
    phi = math.pi * (3. - math.sqrt(5.))  # golden angle in radians

    for i in range(n):
        y = 1 - (i / float(n - 1)) * 2  # y goes from 1 to -1
        radius = math.sqrt(1 - y * y)  # radius at y

        theta = phi * i  # golden angle increment

        x = math.cos(theta) * radius
        z = math.sin(theta) * radius

        points.append((x, y, z))

    return points


def log_normal_proportions(s, loc, scale, xstart, xend, n=10):
    r"""
    Generate a selection of grain
    :return:
    """

    x, dx = np.linspace(xstart, xend, n, retstep=True)

    # x - start/end pairs.
    xses = list(zip(x[:-1], x[1:]))

    xmids = []
    xwidths = []
    pdf_xmids = []
    prob = []
    for xse in xses:
        xmid = (xse[1] + xse[0]) / 2.0
        pdf_xmid = ss.lognorm.pdf(xmid, s, loc=loc, scale=scale)
        p = pdf_xmid * dx

        xmids.append(xmid)
        xwidths.append(dx)
        pdf_xmids.append(pdf_xmid)
        prob.append(p)

    xmids = np.array(xmids)
    xwidths = np.array(xwidths)
    pdf_xmids = np.array(pdf_xmids)

    prob = np.array(prob)
    prob = prob / prob.sum()  # normalize the probabilities so their sum is 1

    return x, prob, xmids, pdf_xmids, xwidths


def main():

    parser = command_line_parser()
    args = parser.parse_args()

    esvd_s = 1.0
    esvd_loc = 20
    esvd_scale = 10
    esvd_start = 20
    esvd_end = 70
    esvd_n = args.nsize

    elong_s = 1.0
    elong_loc = 5
    elong_scale = 10
    elong_start = 5
    elong_end = 100
    elong_n = args.nelong

    dirs_n = args.ndirs
    dirs_prob = [1.0/dirs_n for i in range(dirs_n)]  # Directions are uniformly distributed.

    H = 23.87
    hx = 1.0
    hy = 2.0
    hz = 8.1
    material = "magnetite"

    Tamb = 15.00
    T0 = 579.0
    t1 = 10.0
    T1 = 30.0

    output_pdf = "sd_cooling_s{}_e{}_d{}.pdf".format(esvd_n, elong_n, dirs_n)
    output_csv = "sd_cooling_s{}_e{}_d{}.csv".format(esvd_n, elong_n, dirs_n)

    print("Running cooling model:")
    print("Sizes: {}".format(esvd_n))
    print("Elongations: {}".format(elong_n))
    print("Direction: {}".format(dirs_n))
    print("")
    print("The following output will be produced:")
    print("Output pdf: {}".format(output_pdf))
    print("Output csv: {}".format(output_csv))


    ####################
    # Set up the cooling function
    ####################

    # Cooling function
    cool_fun = sd_cooling.newtonian_temperature_function(Tamb, T0, T1, t1)

    # Derivative of cooling function w.r.t. time
    d_cool_fun_dt = d_newtonian_temperature_function_dt(Tamb, T0, T1, t1)

    # Allowable %age drop in temperature over a time range
    P = 1.0/100

    # The stopping temperature fraction.
    PTstop = 0.1/100

    # The stopping temperature.
    Tstop = Tamb + Tamb*PTstop

    times = [0.0]
    temperatures = [T0]
    dts = []

    tn = 0.0
    Tn = T0
    while Tstop < Tn:
        dt = abs( (P * cool_fun(tn)) / d_cool_fun_dt(tn) )
        tn = tn + dt
        Tn = cool_fun(tn)
        times.append(tn)
        temperatures.append(Tn)
        dts.append(dt)
    dts.append(-1)

    if args.cooling_info_only:
        for i in range(len(times)-1):
            t = times[i]
            T = temperatures[i]
            dt = dts[i]
            print("{:20.15f} s {:20.15f} C {:20.15f} s".format(t, T, dt))

        print("There are {} temperature steps in the model".format(len(times)))
        sys.exit(1)

    if args.real_run:

        # esvd and proportions according to log-normal distribution.
        esvd, esvd_prob, esvd_xmids, esvd_pdf_xmids, esvd_xwidths = log_normal_proportions(esvd_s, esvd_loc, esvd_scale, esvd_start, esvd_end, esvd_n)

        # elong and proportions according to log-normal distribution.
        elong, elong_prob, elong_xmids, elong_pdf_xmids, elong_xwidths = log_normal_proportions(elong_s, elong_loc, elong_scale, elong_start, elong_end, elong_n)

        # directions
        dirs = fibonacci_sphere(n=dirs_n)

        # Construct an assemblage of 1000 Stoner-Wohlfarth grain populations.
        assemblage = sd_cooling.Assemblage(579.0, 1E-12, 0)

        for d, dp in zip(esvd, esvd_prob):
            for e, ep in zip(elong, elong_prob):
                for u, up in zip(dirs, dirs_prob):
                    # print("Adding grain with esvd: {:20.15f}m, elongation: {:4.1f}".format(d*1E-9, e))
                    assemblage.add_stoner_wohlfarth_population(
                        float(d*1E-9), float(e),
                        H, hx, hy, hz,
                        u[0], u[1], u[2],
                        material,
                        1.0, 0.0,
                        float(dp*ep*up)
                    )

        # Run the model

        ts = []
        Ts = []
        pop_mags = []
        pop_mag_paras = []
        pop_mag_perps = []

        equi_mags = []
        equi_mag_paras = []
        equi_mag_perps = []

        for i in progressbar.progressbar(range(len(times)-1)):

            t = times[i]
            T = temperatures[i]
            dt = dts[i]

            assemblage.update_temperature_and_rho(T, dt)

            pop_mag = assemblage.population_magnetization(sd_cooling.projection.none, False)
            pop_mag_para = assemblage.population_magnetization(sd_cooling.projection.parallel, False)
            pop_mag_perp = assemblage.population_magnetization(sd_cooling.projection.perpendicular, False)

            equi_mag = assemblage.equilibrium_magnetization(sd_cooling.projection.none, False)
            equi_mag_para = assemblage.equilibrium_magnetization(sd_cooling.projection.parallel, False)
            equi_mag_perp = assemblage.equilibrium_magnetization(sd_cooling.projection.perpendicular, False)

            # print("equi_mag: {}".format(equi_mag.norm()))

            ts.append(t)
            Ts.append(T)

            pop_mags.append(pop_mag.norm())
            pop_mag_paras.append(pop_mag_para.norm())
            pop_mag_perps.append(pop_mag_perp.norm())

            equi_mags.append(equi_mag.norm())
            equi_mag_paras.append(equi_mag_para.norm())
            equi_mag_perps.append(equi_mag_perp.norm())

        # Save output to a file
        data = {
            "time": ts,
            "temperature": Ts,
            "m cool": pop_mags,
            "m cool parallel": pop_mag_paras,
            "m cool perp": pop_mag_perps,
            "m equi": equi_mags,
            "m equi parallel": equi_mag_paras,
            "m equi perp": equi_mag_perps
        }
        df = pd.DataFrame(data, columns=["time", "temperature", "m cool", "m cool parallel", "m cool perp", "m equi", "m equi parallel", "m equi perp"])
        df.to_csv(output_csv)

        # Plotting routines
        fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2)
        fig.suptitle("A model")

        x = np.linspace(esvd_start, esvd_end, 10000)

        ax1.scatter(Ts, pop_mag_paras)
        ax1.scatter(Ts, equi_mag_paras)
        ax1.set_xlabel("temperature (C)")
        ax1.set_ylabel("magnetization (normalized)")

        ax2.scatter(ts, pop_mag_paras)
        ax2.scatter(ts, equi_mag_paras)
        ax2.set_xlabel("time (s)")
        ax2.set_ylabel("magnetization (normalized)")

        ax3.bar(esvd_xmids, esvd_pdf_xmids, width=esvd_xwidths, edgecolor="darkblue")
        esvd_x = np.linspace(esvd_start, esvd_end, 10000)
        ax3.plot(x, ss.lognorm.pdf(esvd_x, esvd_s, loc=esvd_loc, scale=esvd_scale), '-k')
        ax3.set_title("Size distribution (ESVD)")

        ax4.bar(elong_xmids, elong_pdf_xmids, width=elong_xwidths, edgecolor="darkblue")
        elong_x = np.linspace(elong_start, elong_end, 10000)
        ax4.plot(elong_x, ss.lognorm.pdf(elong_x, elong_s, loc=elong_loc, scale=elong_scale), '-k')
        ax4.set_title("Elongation distribution (%-age)")

        # Show the plot.
        plt.show()
        fig.savefig(output_pdf)



if __name__ == "__main__":
    main()

#!python3

import re
import sys

from argparse import ArgumentParser

import math
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import progressbar
import sd_cooling


def float_to_str(x: float):
    r"""
    d means decimal point, p means plus
    """
    str_flt = "{:5.1e}".format(x)
    str_flt = str_flt.replace(".", "d")
    str_flt = str_flt.replace("+", "p")

    return str_flt


def d_newtonian_temperature_function_dt(Tamb, T0, T1, t1):
    k = (1.0/t1) * math.log((T0 - Tamb)/(T1 - Tamb))
    def dT_dt(t):
        return -1.0 * k * (T0-Tamb) * math.exp(-1.0*k*t)
    return dT_dt


def parse_size(size):
    r"""
    The size should either be a range or a single size.
    :param size: the size to parse, either an integer or a range.
    :return: a list of integers.
    """
    regex_single_size = re.compile(r"([0-9]+)")
    regex_range_size = re.compile(r"([0-9]+):([0-9]+):([0-9]+)")

    match_single_size = regex_single_size.fullmatch(size)
    if match_single_size:
        return [int(match_single_size.group(1))]

    match_range_size = regex_range_size.fullmatch(size)
    if match_range_size:
        start = int(match_range_size.group(1))
        step = int(match_range_size.group(2))
        stop = int(match_range_size.group(3)) + step
        return list(range(start, stop, step))


def parse_elong(elong):
    r"""
    The elongation should either be a range or a single elongation.
    :param elong: the elong to parse, either an integer or a range.
    :return: a list of integers.
    """
    regex_single_elong = re.compile(r"([0-9]+)")
    regex_range_elong = re.compile(r"([0-9]+):([0-9]+):([0-9]+)")

    match_single_elong = regex_single_elong.fullmatch(elong)
    if match_single_elong:
        return [int(match_single_elong.group(1))]

    match_range_elong = regex_range_elong.fullmatch(elong)
    if match_range_elong:
        start = int(match_range_elong.group(1))
        step = int(match_range_elong.group(2))
        stop = int(match_range_elong.group(3)) + step
        return list(range(start, stop, step))


def command_line_parser():
    r"""
    Create a command line parser.
    :return: the comand lline parser.,
    """
    parser = ArgumentParser()

    parser.add_argument("size", type=str, help="the size of the grain")
    parser.add_argument("elong", type=str, help="the percentage elongation")
    parser.add_argument("ndirs", type=int, help="the number of directions")
    parser.add_argument("Tamb", type=float, help="the ambient temperature")
    parser.add_argument("T0", type=float, help="the initial temperature")
    parser.add_argument("t1", type=float, help="some time after heating")
    parser.add_argument("T1", type=float, help="some temperature after heating (corresponding to t1)")
    parser.add_argument("--real-run", action="store_true", help="actually run the models")

    return parser


def fibonacci_sphere(n=1):

    points = []
    phi = math.pi * (3. - math.sqrt(5.))  # golden angle in radians

    for i in range(n):
        y = 1 - (i / float(n - 1)) * 2  # y goes from 1 to -1
        radius = math.sqrt(1 - y * y)   # radius at y

        theta = phi * i  # golden angle increment

        x = math.cos(theta) * radius
        z = math.sin(theta) * radius

        points.append((x, y, z))

    return points


def calculate_cooling_times(Tamb, T0, t1, T1, P=1.0/100, PTStop=0.1/100):
    r"""
    Compute the array of cooling times for a given model.
    :param Tamb: the ambient temperature
    :param T0: the initial temperature
    :param t1: the time at which the cooling curve hits T1
    :param T1: the temperature after a given time t1
    :param P: the allowable percentage drop in temperature between two times
    :param PTStop: the percentage fraction above Tamb at which we stop taking
                   samples from the cooling curve
    :return: an array of times and temperatures that make up a cooling curve
    """
    # Cooling function
    cool_fun = sd_cooling.newtonian_temperature_function(Tamb, T0, T1, t1)

    # Derivative of cooling function w.r.t. time
    d_cool_fun_dt = d_newtonian_temperature_function_dt(Tamb, T0, T1, t1)

    # The stopping temperature (i.e. when we stop the simulation)
    Tstop = Tamb + Tamb*PTStop

    times = [0.0]  # Start our simulation at time t=0.0
    temperatures = [T0]  # Start simulation at temperature T0
    dts = []  # Change in time between t_i and t_{i+1}

    tn = 0.0
    Tn = T0

    while Tstop < Tn:
        dt = abs((P * cool_fun(tn)) / d_cool_fun_dt(tn))
        tn = tn + dt
        Tn = cool_fun(tn)
        times.append(tn)
        temperatures.append(Tn)
        dts.append(dt)
    dts.append(-1)  # Pad the dts array so that it is the same length as the
                    # `times` and `temperatures` array

    return times, temperatures, dts


def main():

    parser = command_line_parser()
    args = parser.parse_args()

    sizes = parse_size(args.size)
    elongs = parse_elong(args.elong)

    dirs_n = args.ndirs
    dirs_prob = [1.0/dirs_n for i in range(dirs_n)]  # Directions are uniformly distributed.

    H = 23.87

    hx = 1.0
    hy = 2.0
    hz = 8.1
    hlen = math.sqrt(hx*hx + hy*hy + hz*hz)
    hx /= hlen
    hy /= hlen
    hz /= hlen

    material = "magnetite"

    ############################################################################
    # Set up the cooling function
    ############################################################################

    times, temperatures, dts = calculate_cooling_times(
        args.Tamb,
        args.T0,
        args.t1,
        args.T1
    )

    if not args.real_run:
        # Print some info for the user and exit.
        print("Cooling model info for '{}'".format(material))
        print("Sizes: {} nm (esvd)".format(sizes))
        print("Elongations: {} %".format(elongs))
        print("Directions: {}".format(dirs_n))
        print("H: {} A/m".format(H))
        print("H direction: {:10.5f}, {:10.5f}, {:10.5f}".format(hx, hy, hz))
        print("T ambient {} C".format(args.Tamb))
        print("T initial {} C".format(args.T0))
        print("T cooling {} C after {} s".format(args.T1, args.t1))
        print("")
        sys.exit()

    ############################################################################
    # This code runs if the real-run flag is set.
    ############################################################################

    # directions
    dirs = fibonacci_sphere(n=dirs_n)

    # Array to hold magnetization values as a function of size/elongation and
    # cooling rate.
    cooled_ms = []
    for size in sizes:
        for elong in elongs:
            print("Running model for {}nm with {}% elongation".format(size, elong))
            # Construct an assemblage of grains with different orientations (but same size/elong)
            assemblage = sd_cooling.Assemblage(args.T0, 1E-12, 0)
            for direction, direction_probability in zip(dirs, dirs_prob):
                # Add a single mono-dispertion.
                assemblage.add_stoner_wohlfarth_population(
                    float(size*1E-9), float(elong),
                    H, hx, hy, hz,
                    direction[0], direction[1], direction[2],
                    material,
                    1.0, 0.0,
                    float(direction_probability)
                )

            ###########################
            # Run the model
            ###########################

            ts = []
            Ts = []
            pop_mags = []
            pop_mag_paras = []
            pop_mag_perps = []

            equi_mags = []
            equi_mag_paras = []
            equi_mag_perps = []

            for i in progressbar.progressbar(range(len(times)-1)):

                t = times[i]
                T = temperatures[i]
                dt = dts[i]

                assemblage.update_temperature_and_rho(T, dt)

                pop_mag = assemblage.population_magnetization(sd_cooling.projection.none, False)
                pop_mag_para = assemblage.population_magnetization(sd_cooling.projection.parallel, False)
                pop_mag_perp = assemblage.population_magnetization(sd_cooling.projection.perpendicular, False)

                equi_mag = assemblage.equilibrium_magnetization(sd_cooling.projection.none, False)
                equi_mag_para = assemblage.equilibrium_magnetization(sd_cooling.projection.parallel, False)
                equi_mag_perp = assemblage.equilibrium_magnetization(sd_cooling.projection.perpendicular, False)

                # print("equi_mag: {}".format(equi_mag.norm()))

                ts.append(t)
                Ts.append(T)

                pop_mags.append(pop_mag.norm())
                pop_mag_paras.append(pop_mag_para.norm())
                pop_mag_perps.append(pop_mag_perp.norm())

                equi_mags.append(equi_mag.norm())
                equi_mag_paras.append(equi_mag_para.norm())
                equi_mag_perps.append(equi_mag_perp.norm())

            ####################################################################
            # Save output to a file
            ####################################################################

            data = {
                "time": ts,
                "temperature": Ts,
                "m cool": pop_mags,
                "m cool parallel": pop_mag_paras,
                "m cool perp": pop_mag_perps,
                "m equi": equi_mags,
                "m equi parallel": equi_mag_paras,
                "m equi perp": equi_mag_perps
            }
            df = pd.DataFrame(data,
                              columns=[
                                  "time",
                                  "temperature",
                                  "m cool",
                                  "m cool parallel",
                                  "m cool perp",
                                  "m equi",
                                  "m equi parallel",
                                  "m equi perp"
                              ])

            ####################################################################
            # Write cooling data for these parameters to a csv file.
            ####################################################################

            output_csv = "sd_cooling_size_{}_elong_{}_tamb_{}_T0_{}_t1_{}_T1_{}_ndirs_{}.csv".format(
                size,
                elong,
                float_to_str(args.Tamb),
                float_to_str(args.T0),
                float_to_str(args.t1),
                float_to_str(args.T1),
                args.ndirs
            )
            df.to_csv(output_csv)

            ####################################################################
            # Write cooling data for these parameters to a pdf file.
            ####################################################################

            output_pdf_time = "sd_cooling_size_{}_elong_{}_tamb_{}_T0_{}_t1_{}_T1_{}_ndirs_{}_time.pdf".format(
                size,
                elong,
                float_to_str(args.Tamb),
                float_to_str(args.T0),
                float_to_str(args.t1),
                float_to_str(args.T1),
                args.ndirs
            )

            fig_time = plt.figure()
            ax_time = fig_time.gca()

            ax_time.plot(ts, pop_mag_paras)
            ax_time.plot(ts, equi_mag_paras)

            # Show the plot.
            fig_time.show()
            fig_time.savefig(output_pdf_time)

            ####################################################################
            # Write cooling data for these parameters to a pdf file.
            ####################################################################

            output_pdf_temp = "sd_cooling_size_{}_elong_{}_tamb_{}_T0_{}_t1_{}_T1_{}_ndirs_{}_temp.pdf".format(
                size,
                elong,
                float_to_str(args.Tamb),
                float_to_str(args.T0),
                float_to_str(args.t1),
                float_to_str(args.T1),
                args.ndirs
            )

            fig_temp = plt.figure()
            ax_temp = fig_temp.gca()

            ax_temp.plot(Ts, pop_mag_paras)
            ax_temp.plot(Ts, equi_mag_paras)

            # Show the plot.
            fig_temp.show()
            fig_temp.savefig(output_pdf_temp)

            ####################################################################
            # Write the final magnetizations to a list.
            ####################################################################

            cooled_ms.append((
                float(size),
                float(elong),
                float(pop_mags[-1]),
                float(pop_mag_paras[-1]),
                float(pop_mag_perps[-1])))

    # Write final magnetisation to a file
    with open("cooling_data_at_rate_{}.csv".format(float_to_str(args.t1)), "w") as fout:
        for r in cooled_ms:
            fout.write("{},{},{},{},{}\n".format(r[0], r[1], r[2], r[3], r[4]))


if __name__ == "__main__":
    main()



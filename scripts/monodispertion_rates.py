#!python3

from argparse import ArgumentParser

import os
import sys
import math
import matplotlib.pyplot as plt
import progressbar

import pandas as pd

from sd_cooling import newtonian_temperature_function
from sd_cooling import d_newtonian_temperature_function_dt
from sd_cooling import Assemblage
from sd_cooling import projection

from sd_cooling.util.command_line import parse_float_list

from sd_cooling.util.formats import float_to_str

from sd_cooling.util.geometry import fibonacci_sphere

from sd_cooling.util.data import lst_to_dataframe


def command_line_parser():
    r"""
    Create a command line parser.
    :return: the command line parser.,
    """
    parser = ArgumentParser()

    parser.add_argument("material", choices=["iron", "magnetite"],
                        help="the name of the material")
    parser.add_argument("size", type=float,
                        help="the size of the grain")
    parser.add_argument("elong", type=float,
                        help="the percentage elongation")
    parser.add_argument("ndirs", type=int,
                        help="the number of directions")
    parser.add_argument("H", type=float,
                        help="the size of the applied field (A/m)")
    parser.add_argument("hx", type=float,
                        help="the x-component of the applied field direction")
    parser.add_argument("hy", type=float,
                        help="the y-component of the applied field direction")
    parser.add_argument("hz", type=float,
                        help="the z-component of the applied field direction")
    parser.add_argument("Tamb", type=float,
                        help="the ambient temperature")
    parser.add_argument("T0", type=float,
                        help="the initial temperature")
    parser.add_argument("T1", type=float,
                        help="reference temperature after cooling for some "
                             "given rate in `rates` (see `rates`)")
    parser.add_argument("rates", type=str,
                        help="comma separated list of rates - this is the time "
                             "(second) that it takes to go from T0 to T1")
    parser.add_argument("data_dir", type=str,
                        help="directory in to which output files are written")
    parser.add_argument("--pstep", type=float, default=0.005,
                        help="allowable drop (as a percentage) in temperature "
                             "within a time step")
    parser.add_argument('--pstop', type=float, default=0.1,
                        help="the amount (as a percentage) above Tamb at which "
                             "the model is halted")
    parser.add_argument("--real-run", action="store_true",
                        help="actually run the models")
    parser.add_argument("--progress-bar", action="store_true",
                        help="display a progress bar if needed")
    parser.add_argument("--quiet", action="store_true",
                        help="display no output as the program executes")

    return parser


def calculate_cooling_times(Tamb, T0, t1, T1, PStep, PTStop):
    r"""
    Compute the array of cooling times for a given model.
    :param Tamb: the ambient temperature
    :param T0: the initial temperature
    :param t1: the time at which the cooling curve hits T1
    :param T1: the temperature after a given time t1
    :param PStep: the allowable percentage drop in temperature between two times
    :param PTStop: the percentage fraction above Tamb at which we stop taking
                   samples from the cooling curve
    :return: an array of times and temperatures that make up a cooling curve
    """
    P = PStep / 100.0
    P_end = PTStop / 100.0

    # Cooling function
    cool_fun = newtonian_temperature_function(Tamb, T0, T1, t1)

    # Derivative of cooling function w.r.t. time
    d_cool_fun_dt = d_newtonian_temperature_function_dt(Tamb, T0, T1, t1)

    # The stopping temperature (i.e. when we stop the simulation)
    Tstop = Tamb + Tamb * P_end

    times = [0.0]  # Start our simulation at time t=0.0
    temperatures = [T0]  # Start simulation at temperature T0
    dts = []  # Change in time between t_i and t_{i+1}

    tn = 0.0
    Tn = T0

    while Tstop < Tn:
        dt = abs((P * cool_fun(tn)) / d_cool_fun_dt(tn))
        tn = tn + dt
        Tn = cool_fun(tn)
        times.append(tn)
        temperatures.append(Tn)
        dts.append(dt)

    # Pad the dts array so that it is the same length as the `times` and
    # `temperatures` array.
    dts.append(-1)

    return times, temperatures, dts


def main():
    parser = command_line_parser()
    args = parser.parse_args()

    # Deal with the progress bar here.
    if args.progress_bar and not args.quiet:
        ProgressBar = progressbar.ProgressBar
    else:
        ProgressBar = progressbar.NullBar

    # Count the number of directions.
    dirs_n = args.ndirs

    # Applied field strength (A/m).
    H = args.H

    # Applied field direction.
    hx = args.hx
    hy = args.hy
    hz = args.hz

    # Normalize the direction (this is done anyway later).
    hlen = math.sqrt(hx * hx + hy * hy + hz * hz)
    hx /= hlen
    hy /= hlen
    hz /= hlen

    # Set the material name.
    material = "magnetite"

    ############################################################################
    # Set up the cooling function
    ############################################################################

    size = args.size
    elong = args.elong

    rates = parse_float_list(args.rates)

    cooled_ms = {
        "log10_t": [],
        "log_t": [],
        "pop_m": [],
        "pop_m_para": [],
        "pop_m_perp": [],
        "pop_ms": [],
        "pop_ms_para": [],
        "pop_ms_perp": []
    }

    if not args.real_run:
        ####################################################################
        # This code runs if real-run flag is not set.
        ####################################################################

        print("Cooling model info for '{}'".format(material))
        print("Sizes: {} nm (esvd)".format(size))
        print("Elongations: {} %".format(elong))
        print("Directions: {}".format(dirs_n))
        print("H: {} A/m".format(H))
        print("H dir: {:10.5f}, {:10.5f}, {:10.5f}".format(hx, hy, hz))
        print("T ambient {} C".format(args.Tamb))
        print("T initial {} C".format(args.T0))
        print("T cooling {} C after rate (below)".format(args.T1))
        print("Percentage step: {}".format(args.pstep))
        print("Percentage stop: {}".format(args.pstop))
        print("Rates:")
        for rate in rates:
            print("\t{:4.2E}".format(rate))
        print("")
        sys.exit(0)

    for t1 in rates:

        times, temperatures, dts = calculate_cooling_times(
            args.Tamb,
            args.T0,
            t1,
            args.T1,
            args.pstep,
            args.pstop
        )

        ####################################################################
        # This code runs if the real-run flag is set.
        ####################################################################

        # directions
        dirs = fibonacci_sphere(n=dirs_n)

        # Each direction probability is uniformly distributed
        dirs_prob = [1.0 / dirs_n for i in range(dirs_n)]

        # Display information for the user.
        if not args.quiet:
            print(
                "Model {}nm & {}% elongation, rate: {:10.5E}s".format(
                    size, elong, t1))

        # Create an assemblage of grains, starting at T0 with epsilon value
        # 1E-12 and zero polishing steps.
        assemblage = Assemblage(args.T0, 1E-12, 0)

        for direction, direction_probability in zip(dirs, dirs_prob):
            # Add a single mono-dispersion of Stoner-Wohlfarth grains.
            assemblage.add_stoner_wohlfarth_population(
                float(size) * 1E-9, float(elong),
                H, hx, hy, hz,
                direction[0], direction[1], direction[2],
                material,
                0.5, 0.5,
                float(direction_probability)
            )

        ###########################
        # Run the model
        ###########################

        ts = []  # times.
        Ts = []  # temperatures.

        # Cooled quantities.
        pop_mags = []  # population magnetisation.
        pop_mags_para = []  # pop. mag. parallel components.
        pop_mags_perp = []  # pop. mag. perpendicular components.

        pop_mags_ms = []  # un-normalized population magnetisation.
        pop_mags_para_ms = []  # un-normed. pop. mag. para. components.
        pop_mags_perp_ms = []  # un-normed. pop. mag. perp. components.

        # Equilibrium quantities.
        equi_mags = []  # population magnetisation.
        equi_mags_para = []  # pop. mag. parallel components.
        equi_mags_perp = []  # pop. mag. perpendicular components.

        equi_mags_ms = []  # un-normalized population magnetisation.
        equi_mags_para_ms = []  # un-normed. pop. mag. para. components.
        equi_mags_perp_ms = []  # un-normed. pop. mag. perp. components.

        bar = ProgressBar()
        for i in bar(range(len(times) - 1)):
            t = times[i]
            T = temperatures[i]
            dt = dts[i]

            assemblage.update_temperature_and_rho(T, dt)

            pop_mag = assemblage.population_magnetization(
                projection.none, False)
            pop_mag_para = assemblage.population_magnetization(
                projection.parallel, False)
            pop_mag_perp = assemblage.population_magnetization(
                projection.perpendicular, False)

            pop_mag_ms = assemblage.population_magnetization(
                projection.none, True)
            pop_mag_para_ms = assemblage.population_magnetization(
                projection.parallel, True)
            pop_mag_perp_ms = assemblage.population_magnetization(
                projection.perpendicular, True)

            equi_mag = assemblage.equilibrium_magnetization(
                projection.none, False)
            equi_mag_para = assemblage.equilibrium_magnetization(
                projection.parallel, False)
            equi_mag_perp = assemblage.equilibrium_magnetization(
                projection.perpendicular, False)

            equi_mag_ms = assemblage.equilibrium_magnetization(
                projection.none, True)
            equi_mag_para_ms = assemblage.equilibrium_magnetization(
                projection.parallel, True)
            equi_mag_perp_ms = assemblage.equilibrium_magnetization(
                projection.perpendicular, True)

            ts.append(t)
            Ts.append(T)

            # Put intensities on arrays.
            pop_mags.append(pop_mag.norm())
            pop_mags_para.append(pop_mag_para.norm())
            pop_mags_perp.append(pop_mag_perp.norm())

            pop_mags_ms.append(pop_mag_ms.norm())
            pop_mags_para_ms.append(pop_mag_para_ms.norm())
            pop_mags_perp_ms.append(pop_mag_perp_ms.norm())

            equi_mags.append(equi_mag.norm())
            equi_mags_para.append(equi_mag_para.norm())
            equi_mags_perp.append(equi_mag_perp.norm())

            equi_mags_ms.append(equi_mag_ms.norm())
            equi_mags_para_ms.append(equi_mag_para_ms.norm())
            equi_mags_perp_ms.append(equi_mag_perp_ms.norm())

        ####################################################################
        # Save output to a file
        ####################################################################

        data = {
            "time": ts,
            "temperature": Ts,
            "m cool": pop_mags,
            "m cool parallel": pop_mags_para,
            "m cool perp": pop_mags_perp,
            "m equi": equi_mags,
            "m equi parallel": equi_mags_para,
            "m equi perp": equi_mags_perp
        }
        df = lst_to_dataframe(
            ts, Ts,
            pop_mags, pop_mags_para, pop_mags_perp,
            equi_mags, equi_mags_para, equi_mags_perp,
            pop_mags_ms, pop_mags_para_ms, pop_mags_perp_ms,
            equi_mags_ms, equi_mags_para_ms, equi_mags_perp_ms)

        ####################################################################
        # Write cooling data for these parameters to a csv file.
        ####################################################################

        output_csv = os.path.join(
            args.data_dir,
            "sd_cooling_size_{}_elong_{}_tamb_{}_T0_{}_t1_{}_T1_{}_ndirs_{}.csv".format(
                size,
                elong,
                float_to_str(args.Tamb),
                float_to_str(args.T0),
                float_to_str(t1),
                float_to_str(args.T1),
                args.ndirs
            )
        )
        df.to_csv(output_csv)

        ####################################################################
        # Write cooling data for these parameters to a pdf file.
        ####################################################################

        output_pdf_time = os.path.join(
            args.data_dir,
            "sd_cooling_size_{}_elong_{}_tamb_{}_T0_{}_t1_{}_T1_{}_ndirs_{}_time.pdf".format(
                size,
                elong,
                float_to_str(args.Tamb),
                float_to_str(args.T0),
                float_to_str(t1),
                float_to_str(args.T1),
                args.ndirs
            )
        )

        fig_time = plt.figure()
        ax_time = fig_time.gca()

        ax_time.plot(ts, pop_mags_para)
        ax_time.plot(ts, equi_mags_para)

        # Show the plot.
        fig_time.savefig(output_pdf_time)
        plt.close(fig_time)

        ####################################################################
        # Write cooling data for these parameters to a pdf file.
        ####################################################################

        output_pdf_temp = os.path.join(
            args.data_dir,
            "sd_cooling_size_{}_elong_{}_tamb_{}_T0_{}_t1_{}_T1_{}_ndirs_{}_temp.pdf".format(
                size,
                elong,
                float_to_str(args.Tamb),
                float_to_str(args.T0),
                float_to_str(t1),
                float_to_str(args.T1),
                args.ndirs
            )
        )

        fig_temp = plt.figure()
        ax_temp = fig_temp.gca()

        ax_temp.plot(Ts, pop_mags_para)
        ax_temp.plot(Ts, equi_mags_para)

        # Show the plot.
        fig_temp.savefig(output_pdf_temp)
        plt.close(fig_temp)

        ####################################################################
        # Write the final magnetizations to a list.
        ####################################################################

        cooled_ms["log10_t"].append(float(math.log(t1, 10)))
        cooled_ms["log_t"].append(float(math.log(t1)))
        cooled_ms["pop_m"].append(float(pop_mags[-1]))
        cooled_ms["pop_m_para"].append(float(pop_mags_para[-1]))
        cooled_ms["pop_m_perp"].append(float(pop_mags_perp[-1]))
        cooled_ms["pop_ms"].append(float(pop_mags_ms[-1]))
        cooled_ms["pop_ms_para"].append(float(pop_mags_para_ms[-1]))
        cooled_ms["pop_ms_perp"].append(float(pop_mags_perp_ms[-1]))

    #
    # Write final magnetisations to a file.
    #
    cooling_file_csv = os.path.join(
        args.data_dir,
        "cooling_rate_data_size_{}_elong_{}.csv".format(
            float_to_str(args.size),
            float_to_str(args.elong)
        )
    )

    cooling_file_pdf = os.path.join(
        args.data_dir,
        "cooling_rate_data_size_{}_elong_{}.pdf".format(
            float_to_str(args.size),
            float_to_str(args.elong)
        )
    )

    # Write CSV file.
    df_cooled = pd.DataFrame(cooled_ms)
    df_cooled.to_csv(cooling_file_csv)

    # Write pdf file.
    fig_cooling = plt.figure()
    ax_temp = fig_cooling.gca()
    ax_temp.plot(
        [log_t for log_t in cooled_ms["log_t"]],
        [pop_m_para for pop_m_para in cooled_ms["pop_m_para"]]
    )
    fig_cooling.savefig(cooling_file_pdf)
    plt.close(fig_cooling)


if __name__ == "__main__":
    main()

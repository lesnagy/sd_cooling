import sd_cooling

import matplotlib.pyplot as plt

def main():
    assemblage = sd_cooling.Assemblage(579.0, 1E-12, 0)

    # Grain parameters.
    esvd = 40E-9
    elong = 30.0
    H = 23.87
    material = "magnetite"

    # Temperature parameters.
    Tamb = 15.00
    T0 = 579.0
    t1 = 10.0
    T1 = 30.00

    cool_fun = sd_cooling.newtonian_temperature_function(Tamb, T0, T1, t1)

    assemblage.add_stoner_wohlfarth_population(
        esvd, elong,
        H, 1.0, 2.0, 8.1,
        1.0, 2.0, 8.0,
        material,
        1.0, 0.0,
        0.5
    )

    assemblage.add_stoner_wohlfarth_population(
        esvd, elong,
        H, 1.0, 2.0, 8.1,
        1.0, 2.0, 8.0,
        material,
        1.0, 0.0,
        0.5
    )

    nsteps = 10000
    dt = t1/(nsteps - 1)

    ts = []
    Ts = []
    pop_mags = []
    pop_mag_paras = []
    pop_mag_perps = []

    equi_mags = []
    equi_mag_paras = []
    equi_mag_perps = []

    for i in range(1):
        t = 0.0 + dt * i
        T = cool_fun(t)

        assemblage.update_temperature_and_rho(T, dt)

        pop_mag = assemblage.population_magnetization(sd_cooling.projection.none, False)
        pop_mag_para = assemblage.population_magnetization(sd_cooling.projection.parallel, False)
        pop_mag_perp = assemblage.population_magnetization(sd_cooling.projection.perpendicular, False)

        equi_mag = assemblage.equilibrium_magnetization(sd_cooling.projection.none, False)
        equi_mag_para = assemblage.equilibrium_magnetization(sd_cooling.projection.parallel, False)
        equi_mag_perp = assemblage.equilibrium_magnetization(sd_cooling.projection.perpendicular, False)

        ts.append(t)
        Ts.append(T)

        pop_mags.append(pop_mag.norm())
        pop_mag_paras.append(pop_mag_para.norm())
        pop_mag_perps.append(pop_mag_perp.norm())

        equi_mags.append(equi_mag.norm())
        equi_mag_paras.append(equi_mag_para.norm())
        equi_mag_perps.append(equi_mag_perp.norm())

    fig, (ax1, ax2) = plt.subplots(2, 1)
    fig.suptitle("Cooling for single domain grains")

    ax1.scatter(Ts, pop_mag_paras)
    ax1.scatter(Ts, equi_mag_paras)
    ax1.set_xlabel("temperature (C)")
    ax1.set_ylabel("magnetization (normalized)")

    ax2.scatter(ts, pop_mag_paras)
    ax2.scatter(ts, equi_mag_paras)
    ax2.set_xlabel("time (s)")
    ax2.set_ylabel("magnetization (normalized)")
    
    plt.show()


if __name__ == "__main__":
    main()

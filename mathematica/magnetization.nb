(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 12.1' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     29739,        772]
NotebookOptionsPosition[     28273,        741]
NotebookOutlinePosition[     28717,        758]
CellTagsIndexPosition[     28674,        755]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"DrawMagnetization", "[", "\[Theta]_", "]"}], ":=", 
   RowBox[{"Module", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
      "R", ",", "u", ",", "h", ",", "v", ",", "vx", ",", "vy", ",", "vz", ",",
        "m"}], "}"}], ",", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"u", "=", 
       RowBox[{"Normalize", "[", 
        RowBox[{"{", 
         RowBox[{"1", ",", "2", ",", 
          RowBox[{"-", "2"}]}], "}"}], "]"}]}], ";", "\[IndentingNewLine]", 
      RowBox[{"h", "=", 
       RowBox[{"Normalize", "[", 
        RowBox[{"{", 
         RowBox[{"1", ",", "2", ",", "8"}], "}"}], "]"}]}], ";", 
      "\[IndentingNewLine]", "\[IndentingNewLine]", 
      RowBox[{"(*", " ", 
       RowBox[{
       "u", " ", "x", " ", "h", " ", "defines", " ", "the", " ", "planar", 
        " ", "normal"}], " ", "*)"}], "\[IndentingNewLine]", 
      RowBox[{"v", "=", 
       RowBox[{"Normalize", "[", 
        RowBox[{"Cross", "[", 
         RowBox[{"u", ",", "h"}], "]"}], "]"}]}], ";", "\[IndentingNewLine]", 
      "\[IndentingNewLine]", 
      RowBox[{"vx", "=", 
       RowBox[{"v", "[", 
        RowBox[{"[", "1", "]"}], "]"}]}], ";", "\[IndentingNewLine]", 
      RowBox[{"vy", "=", 
       RowBox[{"v", "[", 
        RowBox[{"[", "2", "]"}], "]"}]}], ";", "\[IndentingNewLine]", 
      RowBox[{"vz", "=", 
       RowBox[{"v", "[", 
        RowBox[{"[", "3", "]"}], "]"}]}], ";", "\[IndentingNewLine]", 
      "\[IndentingNewLine]", 
      RowBox[{"R", "=", 
       RowBox[{"(", "\[NoBreak]", GridBox[{
          {
           RowBox[{
            RowBox[{"Cos", "[", "\[Theta]", "]"}], "+", 
            RowBox[{
             SuperscriptBox["vx", "2"], 
             RowBox[{"(", 
              RowBox[{"1", "-", 
               RowBox[{"Cos", "[", "\[Theta]", "]"}]}], ")"}]}]}], 
           RowBox[{
            RowBox[{"vx", " ", "vy", 
             RowBox[{"(", 
              RowBox[{"1", "-", 
               RowBox[{"Cos", "[", "\[Theta]", "]"}]}], ")"}]}], "-", 
            RowBox[{"vz", " ", 
             RowBox[{"Sin", "[", "\[Theta]", "]"}]}]}], 
           RowBox[{
            RowBox[{"vx", " ", "vz", " ", 
             RowBox[{"(", 
              RowBox[{"1", "-", 
               RowBox[{"Cos", "[", "\[Theta]", "]"}]}], ")"}]}], "+", 
            RowBox[{"vy", " ", 
             RowBox[{"Sin", "[", "\[Theta]", "]"}]}]}]},
          {
           RowBox[{
            RowBox[{"vy", " ", "vx", 
             RowBox[{"(", 
              RowBox[{"1", "-", 
               RowBox[{"Cos", "[", "\[Theta]", "]"}]}], ")"}]}], "+", 
            RowBox[{"vz", " ", 
             RowBox[{"Sin", "[", "\[Theta]", "]"}]}]}], 
           RowBox[{
            RowBox[{"Cos", "[", "\[Theta]", "]"}], "+", 
            RowBox[{
             SuperscriptBox["vy", "2"], 
             RowBox[{"(", 
              RowBox[{"1", "-", 
               RowBox[{"Cos", "[", "\[Theta]", "]"}]}], ")"}]}]}], 
           RowBox[{
            RowBox[{"vy", " ", "vz", 
             RowBox[{"(", 
              RowBox[{"1", "-", 
               RowBox[{"Cos", "[", "\[Theta]", "]"}]}], ")"}]}], "-", 
            RowBox[{"vx", " ", 
             RowBox[{"Sin", "[", "\[Theta]", "]"}]}]}]},
          {
           RowBox[{
            RowBox[{"vz", " ", "vx", " ", 
             RowBox[{"(", 
              RowBox[{"1", "-", 
               RowBox[{"Cos", "[", "\[Theta]", "]"}]}], ")"}]}], "-", 
            RowBox[{"vy", " ", 
             RowBox[{"Sin", "[", "\[Theta]", "]"}]}]}], 
           RowBox[{
            RowBox[{"vz", " ", "vy", 
             RowBox[{"(", 
              RowBox[{"1", "-", 
               RowBox[{"Cos", "[", "\[Theta]", "]"}]}], ")"}]}], "+", 
            RowBox[{"vx", " ", 
             RowBox[{"Sin", "[", "\[Theta]", "]"}]}]}], 
           RowBox[{
            RowBox[{"Cos", "[", "\[Theta]", "]"}], "+", 
            RowBox[{
             SuperscriptBox["vz", "2"], 
             RowBox[{"(", 
              RowBox[{"1", "-", 
               RowBox[{"Cos", "[", "\[Theta]", "]"}]}], ")"}]}]}]}
         }], "\[NoBreak]", ")"}]}], ";", "\[IndentingNewLine]", 
      "\[IndentingNewLine]", 
      RowBox[{"(*", " ", 
       RowBox[{
       "m", " ", "is", " ", "u", " ", "rotated", " ", "by", " ", "\[Theta]", 
        " ", "in", " ", "the", " ", "plane", " ", "defined", " ", "by", " ", 
        "u", " ", "x", " ", "h"}], " ", "*)"}], "\[IndentingNewLine]", 
      RowBox[{"m", " ", "=", " ", 
       RowBox[{"FullSimplify", "[", 
        RowBox[{"R", ".", "u"}], "]"}]}], ";", "\[IndentingNewLine]", 
      "\[IndentingNewLine]", 
      RowBox[{"Show", "[", 
       RowBox[{"{", "\[IndentingNewLine]", 
        RowBox[{
         RowBox[{"Graphics3D", "[", 
          RowBox[{"{", " ", 
           RowBox[{"Red", ",", 
            RowBox[{"Arrowheads", "[", "0.05", "]"}], ",", 
            "\[IndentingNewLine]", 
            RowBox[{"Arrow", "[", 
             RowBox[{"Tube", "[", 
              RowBox[{"{", 
               RowBox[{
                RowBox[{"{", 
                 RowBox[{
                  RowBox[{"-", "1"}], ",", "0", ",", "0"}], "}"}], ",", 
                RowBox[{"{", 
                 RowBox[{"1", ",", "0", ",", "0"}], "}"}]}], "}"}], "]"}], 
             "]"}]}], "}"}], "]"}], ",", "\[IndentingNewLine]", 
         "\[IndentingNewLine]", 
         RowBox[{"Graphics3D", "[", 
          RowBox[{"{", 
           RowBox[{"Blue", ",", 
            RowBox[{"Arrowheads", "[", "0.05", "]"}], ",", 
            "\[IndentingNewLine]", 
            RowBox[{"Arrow", "[", 
             RowBox[{"Tube", "[", 
              RowBox[{"{", 
               RowBox[{
                RowBox[{"{", 
                 RowBox[{"0", ",", 
                  RowBox[{"-", "1"}], ",", "0"}], "}"}], ",", 
                RowBox[{"{", 
                 RowBox[{"0", ",", "1", ",", "0"}], "}"}]}], "}"}], "]"}], 
             "]"}]}], "}"}], "]"}], ",", "\[IndentingNewLine]", 
         "\[IndentingNewLine]", 
         RowBox[{"Graphics3D", "[", 
          RowBox[{"{", 
           RowBox[{"Yellow", ",", 
            RowBox[{"Arrowheads", "[", "0.05", "]"}], ",", 
            "\[IndentingNewLine]", 
            RowBox[{"Arrow", "[", 
             RowBox[{"Tube", "[", 
              RowBox[{"{", 
               RowBox[{
                RowBox[{"{", 
                 RowBox[{"0", ",", "0", ",", 
                  RowBox[{"-", "1"}]}], "}"}], ",", 
                RowBox[{"{", 
                 RowBox[{"0", ",", "0", ",", "1"}], "}"}]}], "}"}], "]"}], 
             "]"}]}], "}"}], "]"}], ",", "\[IndentingNewLine]", 
         "\[IndentingNewLine]", 
         RowBox[{"Graphics3D", "[", 
          RowBox[{"{", 
           RowBox[{"Black", ",", 
            RowBox[{"Arrowheads", "[", "0.05", "]"}], ",", 
            "\[IndentingNewLine]", 
            RowBox[{"Arrow", "[", 
             RowBox[{"Tube", "[", 
              RowBox[{"{", 
               RowBox[{
                RowBox[{"{", 
                 RowBox[{"0", ",", "0", ",", "0"}], "}"}], ",", "u"}], "}"}], 
              "]"}], "]"}]}], "}"}], "]"}], ",", "\[IndentingNewLine]", 
         "\[IndentingNewLine]", 
         RowBox[{"Graphics3D", "[", 
          RowBox[{"{", 
           RowBox[{"Gray", ",", 
            RowBox[{"Arrowheads", "[", "0.05", "]"}], ",", 
            "\[IndentingNewLine]", 
            RowBox[{"Arrow", "[", 
             RowBox[{"Tube", "[", 
              RowBox[{"{", 
               RowBox[{
                RowBox[{"{", 
                 RowBox[{"0", ",", "0", ",", "0"}], "}"}], ",", "h"}], "}"}], 
              "]"}], "]"}]}], "}"}], "]"}], ",", "\[IndentingNewLine]", 
         "\[IndentingNewLine]", 
         RowBox[{"Graphics3D", "[", 
          RowBox[{"{", 
           RowBox[{"Pink", ",", 
            RowBox[{"Arrowheads", "[", "0.05", "]"}], ",", 
            "\[IndentingNewLine]", 
            RowBox[{"Arrow", "[", 
             RowBox[{"Tube", "[", 
              RowBox[{"{", 
               RowBox[{
                RowBox[{"{", 
                 RowBox[{"0", ",", "0", ",", "0"}], "}"}], ",", "m"}], "}"}], 
              "]"}], "]"}]}], "}"}], "]"}]}], "\[IndentingNewLine]", 
        "\[IndentingNewLine]", "}"}], "]"}]}]}], "\[IndentingNewLine]", 
    "]"}]}], "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"Animate", "[", 
   RowBox[{
    RowBox[{"DrawMagnetization", "[", 
     RowBox[{"\[Theta]", "*", 
      FractionBox["\[Pi]", "180"]}], "]"}], ",", 
    RowBox[{"{", 
     RowBox[{"\[Theta]", ",", "0", ",", "360"}], "}"}]}], "]"}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]"}], "Input",
 CellChangeTimes->{{3.8135754094069805`*^9, 3.8135757032916236`*^9}, {
  3.81357578660509*^9, 3.8135758266441946`*^9}, {3.813575871738555*^9, 
  3.813575912957758*^9}, {3.8135759544189873`*^9, 3.813576320017659*^9}, {
  3.81357635131752*^9, 3.813576374148244*^9}, {3.8135764178738875`*^9, 
  3.8135770134272833`*^9}, {3.813577046162858*^9, 3.8135771190841026`*^9}, {
  3.813577160483799*^9, 3.813577195728757*^9}},
 CellLabel->"In[40]:=",ExpressionUUID->"4fb5e836-44c1-496b-9fc1-5d8a1d36272b"],

Cell[BoxData[
 TagBox[
  StyleBox[
   DynamicModuleBox[{$CellContext`\[Theta]$$ = 301.2341651916504, 
    Typeset`show$$ = True, Typeset`bookmarkList$$ = {}, 
    Typeset`bookmarkMode$$ = "Menu", Typeset`animator$$, Typeset`animvar$$ = 
    1, Typeset`name$$ = "\"untitled\"", Typeset`specs$$ = {{
      Hold[$CellContext`\[Theta]$$], 0, 360}}, Typeset`size$$ = {
    360., {177., 183.}}, Typeset`update$$ = 0, Typeset`initDone$$, 
    Typeset`skipInitDone$$ = True}, 
    DynamicBox[Manipulate`ManipulateBoxes[
     1, StandardForm, "Variables" :> {$CellContext`\[Theta]$$ = 0}, 
      "ControllerVariables" :> {}, 
      "OtherVariables" :> {
       Typeset`show$$, Typeset`bookmarkList$$, Typeset`bookmarkMode$$, 
        Typeset`animator$$, Typeset`animvar$$, Typeset`name$$, 
        Typeset`specs$$, Typeset`size$$, Typeset`update$$, Typeset`initDone$$,
         Typeset`skipInitDone$$}, 
      "Body" :> $CellContext`DrawMagnetization[$CellContext`\[Theta]$$ (Pi/
         180)], "Specifications" :> {{$CellContext`\[Theta]$$, 0, 360, 
         AppearanceElements -> {
          "ProgressSlider", "PlayPauseButton", "FasterSlowerButtons", 
           "DirectionButton"}}}, 
      "Options" :> {
       ControlType -> Animator, AppearanceElements -> None, DefaultBaseStyle -> 
        "Animate", DefaultLabelStyle -> "AnimateLabel", SynchronousUpdating -> 
        True, ShrinkingDelay -> 10.}, "DefaultOptions" :> {}],
     ImageSizeCache->{411., {216., 222.}},
     SingleEvaluation->True],
    Deinitialization:>None,
    DynamicModuleValues:>{},
    SynchronousInitialization->True,
    UndoTrackedVariables:>{Typeset`show$$, Typeset`bookmarkMode$$},
    UnsavedVariables:>{Typeset`initDone$$},
    UntrackedVariables:>{Typeset`size$$}], "Animate",
   Deployed->True,
   StripOnInput->False],
  Manipulate`InterpretManipulate[1]]], "Output",
 CellChangeTimes->{{3.813577101975809*^9, 3.813577120471348*^9}, 
   3.8135771973364573`*^9, 3.814009090343381*^9, 3.8152132049942007`*^9},
 CellLabel->"Out[41]=",ExpressionUUID->"53fe535e-9311-4347-aae1-c657d04065d8"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"R", "=", 
  RowBox[{
   RowBox[{
    RowBox[{"(", "\[NoBreak]", GridBox[{
       {
        RowBox[{
         RowBox[{"Cos", "[", "\[Theta]", "]"}], "+", 
         RowBox[{
          SuperscriptBox["vx", "2"], 
          RowBox[{"(", 
           RowBox[{"1", "-", 
            RowBox[{"Cos", "[", "\[Theta]", "]"}]}], ")"}]}]}], 
        RowBox[{
         RowBox[{"vx", " ", "vy", 
          RowBox[{"(", 
           RowBox[{"1", "-", 
            RowBox[{"Cos", "[", "\[Theta]", "]"}]}], ")"}]}], "-", 
         RowBox[{"vz", " ", 
          RowBox[{"Sin", "[", "\[Theta]", "]"}]}]}], 
        RowBox[{
         RowBox[{"vx", " ", "vz", " ", 
          RowBox[{"(", 
           RowBox[{"1", "-", 
            RowBox[{"Cos", "[", "\[Theta]", "]"}]}], ")"}]}], "+", 
         RowBox[{"vy", " ", 
          RowBox[{"Sin", "[", "\[Theta]", "]"}]}]}]},
       {
        RowBox[{
         RowBox[{"vy", " ", "vx", 
          RowBox[{"(", 
           RowBox[{"1", "-", 
            RowBox[{"Cos", "[", "\[Theta]", "]"}]}], ")"}]}], "+", 
         RowBox[{"vz", " ", 
          RowBox[{"Sin", "[", "\[Theta]", "]"}]}]}], 
        RowBox[{
         RowBox[{"Cos", "[", "\[Theta]", "]"}], "+", 
         RowBox[{
          SuperscriptBox["vy", "2"], 
          RowBox[{"(", 
           RowBox[{"1", "-", 
            RowBox[{"Cos", "[", "\[Theta]", "]"}]}], ")"}]}]}], 
        RowBox[{
         RowBox[{"vy", " ", "vz", 
          RowBox[{"(", 
           RowBox[{"1", "-", 
            RowBox[{"Cos", "[", "\[Theta]", "]"}]}], ")"}]}], "-", 
         RowBox[{"vx", " ", 
          RowBox[{"Sin", "[", "\[Theta]", "]"}]}]}]},
       {
        RowBox[{
         RowBox[{"vz", " ", "vx", " ", 
          RowBox[{"(", 
           RowBox[{"1", "-", 
            RowBox[{"Cos", "[", "\[Theta]", "]"}]}], ")"}]}], "-", 
         RowBox[{"vy", " ", 
          RowBox[{"Sin", "[", "\[Theta]", "]"}]}]}], 
        RowBox[{
         RowBox[{"vz", " ", "vy", 
          RowBox[{"(", 
           RowBox[{"1", "-", 
            RowBox[{"Cos", "[", "\[Theta]", "]"}]}], ")"}]}], "+", 
         RowBox[{"vx", " ", 
          RowBox[{"Sin", "[", "\[Theta]", "]"}]}]}], 
        RowBox[{
         RowBox[{"Cos", "[", "\[Theta]", "]"}], "+", 
         RowBox[{
          SuperscriptBox["vz", "2"], 
          RowBox[{"(", 
           RowBox[{"1", "-", 
            RowBox[{"Cos", "[", "\[Theta]", "]"}]}], ")"}]}]}]}
      }], "\[NoBreak]", ")"}], "/.", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"vx", "\[Rule]", 
       RowBox[{"ax", "[", 
        RowBox[{"[", "1", "]"}], "]"}]}], ",", 
      RowBox[{"vy", "\[Rule]", 
       RowBox[{"ax", "[", 
        RowBox[{"[", "2", "]"}], "]"}]}], ",", 
      RowBox[{"vz", "\[Rule]", 
       RowBox[{"ax", "[", 
        RowBox[{"[", "3", "]"}], "]"}]}]}], "}"}]}], "//", 
   "MatrixForm"}]}]], "Input",
 CellChangeTimes->{{3.8140715837736034`*^9, 3.8140716186626043`*^9}},
 CellLabel->"In[42]:=",ExpressionUUID->"a0b0cac9-69e6-4879-b1ea-d26afbd9bc35"],

Cell[BoxData[
 TemplateBox[{
  "Part", "partd", 
   "\"Part specification \\!\\(\\*RowBox[{\\\"ax\\\", \
\\\"\[LeftDoubleBracket]\\\", \\\"1\\\", \\\"\[RightDoubleBracket]\\\"}]\\) \
is longer than depth of object.\"", 2, 42, 1, 24932412218357064900, "Local"},
  "MessageTemplate"]], "Message", "MSG",
 CellChangeTimes->{{3.8140716070091105`*^9, 3.814071618993718*^9}, 
   3.8152132052405424`*^9},
 CellLabel->
  "During evaluation of \
In[42]:=",ExpressionUUID->"80b28fbb-e146-4c71-a61d-0f7e5d290dcd"],

Cell[BoxData[
 TemplateBox[{
  "Part", "partd", 
   "\"Part specification \\!\\(\\*RowBox[{\\\"ax\\\", \
\\\"\[LeftDoubleBracket]\\\", \\\"2\\\", \\\"\[RightDoubleBracket]\\\"}]\\) \
is longer than depth of object.\"", 2, 42, 2, 24932412218357064900, "Local"},
  "MessageTemplate"]], "Message", "MSG",
 CellChangeTimes->{{3.8140716070091105`*^9, 3.814071618993718*^9}, 
   3.815213205252509*^9},
 CellLabel->
  "During evaluation of \
In[42]:=",ExpressionUUID->"ede53229-aa87-4665-9947-70f9552a026c"],

Cell[BoxData[
 TemplateBox[{
  "Part", "partd", 
   "\"Part specification \\!\\(\\*RowBox[{\\\"ax\\\", \
\\\"\[LeftDoubleBracket]\\\", \\\"3\\\", \\\"\[RightDoubleBracket]\\\"}]\\) \
is longer than depth of object.\"", 2, 42, 3, 24932412218357064900, "Local"},
  "MessageTemplate"]], "Message", "MSG",
 CellChangeTimes->{{3.8140716070091105`*^9, 3.814071618993718*^9}, 
   3.815213205260488*^9},
 CellLabel->
  "During evaluation of \
In[42]:=",ExpressionUUID->"8e30217c-634f-4031-afb9-528fcc2b57c9"],

Cell[BoxData[
 TemplateBox[{
  "General", "stop", 
   "\"Further output of \\!\\(\\*StyleBox[RowBox[{\\\"Part\\\", \\\"::\\\", \
\\\"partd\\\"}], \\\"MessageName\\\"]\\) will be suppressed during this \
calculation.\"", 2, 42, 4, 24932412218357064900, "Local"},
  "MessageTemplate"]], "Message", "MSG",
 CellChangeTimes->{{3.8140716070091105`*^9, 3.814071618993718*^9}, 
   3.815213205272456*^9},
 CellLabel->
  "During evaluation of \
In[42]:=",ExpressionUUID->"42feae5f-bdd5-40e3-a210-a018a31811ee"],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {
      RowBox[{
       RowBox[{"Cos", "[", "\[Theta]", "]"}], "+", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"1", "-", 
          RowBox[{"Cos", "[", "\[Theta]", "]"}]}], ")"}], " ", 
        SuperscriptBox[
         RowBox[{"ax", "\[LeftDoubleBracket]", "1", "\[RightDoubleBracket]"}],
          "2"]}]}], 
      RowBox[{
       RowBox[{
        RowBox[{"(", 
         RowBox[{"1", "-", 
          RowBox[{"Cos", "[", "\[Theta]", "]"}]}], ")"}], " ", 
        RowBox[{"ax", "\[LeftDoubleBracket]", "1", "\[RightDoubleBracket]"}], 
        " ", 
        RowBox[{
        "ax", "\[LeftDoubleBracket]", "2", "\[RightDoubleBracket]"}]}], "-", 
       RowBox[{
        RowBox[{"ax", "\[LeftDoubleBracket]", "3", "\[RightDoubleBracket]"}], 
        " ", 
        RowBox[{"Sin", "[", "\[Theta]", "]"}]}]}], 
      RowBox[{
       RowBox[{
        RowBox[{"(", 
         RowBox[{"1", "-", 
          RowBox[{"Cos", "[", "\[Theta]", "]"}]}], ")"}], " ", 
        RowBox[{"ax", "\[LeftDoubleBracket]", "1", "\[RightDoubleBracket]"}], 
        " ", 
        RowBox[{
        "ax", "\[LeftDoubleBracket]", "3", "\[RightDoubleBracket]"}]}], "+", 
       RowBox[{
        RowBox[{"ax", "\[LeftDoubleBracket]", "2", "\[RightDoubleBracket]"}], 
        " ", 
        RowBox[{"Sin", "[", "\[Theta]", "]"}]}]}]},
     {
      RowBox[{
       RowBox[{
        RowBox[{"(", 
         RowBox[{"1", "-", 
          RowBox[{"Cos", "[", "\[Theta]", "]"}]}], ")"}], " ", 
        RowBox[{"ax", "\[LeftDoubleBracket]", "1", "\[RightDoubleBracket]"}], 
        " ", 
        RowBox[{
        "ax", "\[LeftDoubleBracket]", "2", "\[RightDoubleBracket]"}]}], "+", 
       RowBox[{
        RowBox[{"ax", "\[LeftDoubleBracket]", "3", "\[RightDoubleBracket]"}], 
        " ", 
        RowBox[{"Sin", "[", "\[Theta]", "]"}]}]}], 
      RowBox[{
       RowBox[{"Cos", "[", "\[Theta]", "]"}], "+", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"1", "-", 
          RowBox[{"Cos", "[", "\[Theta]", "]"}]}], ")"}], " ", 
        SuperscriptBox[
         RowBox[{"ax", "\[LeftDoubleBracket]", "2", "\[RightDoubleBracket]"}],
          "2"]}]}], 
      RowBox[{
       RowBox[{
        RowBox[{"(", 
         RowBox[{"1", "-", 
          RowBox[{"Cos", "[", "\[Theta]", "]"}]}], ")"}], " ", 
        RowBox[{"ax", "\[LeftDoubleBracket]", "2", "\[RightDoubleBracket]"}], 
        " ", 
        RowBox[{
        "ax", "\[LeftDoubleBracket]", "3", "\[RightDoubleBracket]"}]}], "-", 
       RowBox[{
        RowBox[{"ax", "\[LeftDoubleBracket]", "1", "\[RightDoubleBracket]"}], 
        " ", 
        RowBox[{"Sin", "[", "\[Theta]", "]"}]}]}]},
     {
      RowBox[{
       RowBox[{
        RowBox[{"(", 
         RowBox[{"1", "-", 
          RowBox[{"Cos", "[", "\[Theta]", "]"}]}], ")"}], " ", 
        RowBox[{"ax", "\[LeftDoubleBracket]", "1", "\[RightDoubleBracket]"}], 
        " ", 
        RowBox[{
        "ax", "\[LeftDoubleBracket]", "3", "\[RightDoubleBracket]"}]}], "-", 
       RowBox[{
        RowBox[{"ax", "\[LeftDoubleBracket]", "2", "\[RightDoubleBracket]"}], 
        " ", 
        RowBox[{"Sin", "[", "\[Theta]", "]"}]}]}], 
      RowBox[{
       RowBox[{
        RowBox[{"(", 
         RowBox[{"1", "-", 
          RowBox[{"Cos", "[", "\[Theta]", "]"}]}], ")"}], " ", 
        RowBox[{"ax", "\[LeftDoubleBracket]", "2", "\[RightDoubleBracket]"}], 
        " ", 
        RowBox[{
        "ax", "\[LeftDoubleBracket]", "3", "\[RightDoubleBracket]"}]}], "+", 
       RowBox[{
        RowBox[{"ax", "\[LeftDoubleBracket]", "1", "\[RightDoubleBracket]"}], 
        " ", 
        RowBox[{"Sin", "[", "\[Theta]", "]"}]}]}], 
      RowBox[{
       RowBox[{"Cos", "[", "\[Theta]", "]"}], "+", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"1", "-", 
          RowBox[{"Cos", "[", "\[Theta]", "]"}]}], ")"}], " ", 
        SuperscriptBox[
         RowBox[{"ax", "\[LeftDoubleBracket]", "3", "\[RightDoubleBracket]"}],
          "2"]}]}]}
    },
    GridBoxAlignment->{"Columns" -> {{Center}}, "Rows" -> {{Baseline}}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.7]}, 
        Offset[0.27999999999999997`]}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}}], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{{3.814071607066955*^9, 3.8140716190356073`*^9}, 
   3.8152132052894106`*^9},
 CellLabel->
  "Out[42]//MatrixForm=",ExpressionUUID->"c13c9f82-9ec8-4256-89a6-\
00ba09de7944"]
}, Open  ]],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.815213224937846*^9, 
  3.815213225599079*^9}},ExpressionUUID->"13feac70-d460-4b88-b187-\
4dc94c7d2f43"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{"Module", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"u", ",", "v", ",", "para", ",", "perp"}], "}"}], ",", 
    "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"u", "=", 
      RowBox[{"{", 
       RowBox[{"2", ",", "3", ",", "1"}], "}"}]}], ";", "\[IndentingNewLine]", 
     RowBox[{"v", "=", 
      RowBox[{"{", 
       RowBox[{"3", ",", "4", ",", 
        RowBox[{"-", "1"}]}], "}"}]}], ";", "\[IndentingNewLine]", 
     RowBox[{"para", "=", 
      RowBox[{"N", "[", 
       RowBox[{
        RowBox[{
         FractionBox[
          RowBox[{"u", ".", "v"}], 
          SuperscriptBox[
           RowBox[{"Norm", "[", "u", "]"}], "2"]], "u"}], ",", "20"}], 
       "]"}]}], ";", "\[IndentingNewLine]", 
     RowBox[{"perp", "=", 
      RowBox[{"N", "[", 
       RowBox[{
        RowBox[{"v", "-", 
         RowBox[{
          FractionBox[
           RowBox[{"u", ".", "v"}], 
           SuperscriptBox[
            RowBox[{"Norm", "[", "u", "]"}], "2"]], "u"}]}], ",", "20"}], 
       "]"}]}], ";", "\[IndentingNewLine]", 
     RowBox[{"Show", "[", 
      RowBox[{"{", "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"Graphics3D", "[", 
         RowBox[{"{", " ", 
          RowBox[{"Red", ",", 
           RowBox[{"Arrowheads", "[", "0.05", "]"}], ",", 
           "\[IndentingNewLine]", 
           RowBox[{"Arrow", "[", 
            RowBox[{"Tube", "[", 
             RowBox[{"{", 
              RowBox[{
               RowBox[{"{", 
                RowBox[{
                 RowBox[{"-", "1"}], ",", "0", ",", "0"}], "}"}], ",", 
               RowBox[{"{", 
                RowBox[{"1", ",", "0", ",", "0"}], "}"}]}], "}"}], "]"}], 
            "]"}]}], "}"}], "]"}], ",", "\[IndentingNewLine]", 
        "\[IndentingNewLine]", 
        RowBox[{"Graphics3D", "[", 
         RowBox[{"{", 
          RowBox[{"Blue", ",", 
           RowBox[{"Arrowheads", "[", "0.05", "]"}], ",", 
           "\[IndentingNewLine]", 
           RowBox[{"Arrow", "[", 
            RowBox[{"Tube", "[", 
             RowBox[{"{", 
              RowBox[{
               RowBox[{"{", 
                RowBox[{"0", ",", 
                 RowBox[{"-", "1"}], ",", "0"}], "}"}], ",", 
               RowBox[{"{", 
                RowBox[{"0", ",", "1", ",", "0"}], "}"}]}], "}"}], "]"}], 
            "]"}]}], "}"}], "]"}], ",", "\[IndentingNewLine]", 
        "\[IndentingNewLine]", 
        RowBox[{"Graphics3D", "[", 
         RowBox[{"{", 
          RowBox[{"Yellow", ",", 
           RowBox[{"Arrowheads", "[", "0.05", "]"}], ",", 
           "\[IndentingNewLine]", 
           RowBox[{"Arrow", "[", 
            RowBox[{"Tube", "[", 
             RowBox[{"{", 
              RowBox[{
               RowBox[{"{", 
                RowBox[{"0", ",", "0", ",", 
                 RowBox[{"-", "1"}]}], "}"}], ",", 
               RowBox[{"{", 
                RowBox[{"0", ",", "0", ",", "1"}], "}"}]}], "}"}], "]"}], 
            "]"}]}], "}"}], "]"}], ",", "\[IndentingNewLine]", 
        "\[IndentingNewLine]", 
        RowBox[{"Graphics3D", "[", 
         RowBox[{"{", 
          RowBox[{"Black", ",", 
           RowBox[{"Arrowheads", "[", "0.05", "]"}], ",", 
           "\[IndentingNewLine]", 
           RowBox[{"Arrow", "[", 
            RowBox[{"Tube", "[", 
             RowBox[{"{", 
              RowBox[{
               RowBox[{"{", 
                RowBox[{"0", ",", "0", ",", "0"}], "}"}], ",", "u"}], "}"}], 
             "]"}], "]"}]}], "}"}], "]"}], ",", "\[IndentingNewLine]", 
        "\[IndentingNewLine]", 
        RowBox[{"Graphics3D", "[", 
         RowBox[{"{", 
          RowBox[{"Gray", ",", 
           RowBox[{"Arrowheads", "[", "0.05", "]"}], ",", 
           "\[IndentingNewLine]", 
           RowBox[{"Arrow", "[", 
            RowBox[{"Tube", "[", 
             RowBox[{"{", 
              RowBox[{
               RowBox[{"{", 
                RowBox[{"0", ",", "0", ",", "0"}], "}"}], ",", "v"}], "}"}], 
             "]"}], "]"}]}], "}"}], "]"}], ",", "\[IndentingNewLine]", 
        "\[IndentingNewLine]", 
        RowBox[{"Graphics3D", "[", 
         RowBox[{"{", 
          RowBox[{"Pink", ",", 
           RowBox[{"Arrowheads", "[", "0.05", "]"}], ",", 
           "\[IndentingNewLine]", 
           RowBox[{"Arrow", "[", 
            RowBox[{"Tube", "[", 
             RowBox[{"{", 
              RowBox[{
               RowBox[{"{", 
                RowBox[{"0", ",", "0", ",", "0"}], "}"}], ",", "para"}], 
              "}"}], "]"}], "]"}]}], "}"}], "]"}], ",", "\[IndentingNewLine]",
         "\[IndentingNewLine]", 
        RowBox[{"Graphics3D", "[", 
         RowBox[{"{", 
          RowBox[{"LightBlue", ",", 
           RowBox[{"Arrowheads", "[", "0.05", "]"}], ",", 
           "\[IndentingNewLine]", 
           RowBox[{"Arrow", "[", 
            RowBox[{"Tube", "[", 
             RowBox[{"{", 
              RowBox[{
               RowBox[{"{", 
                RowBox[{"0", ",", "0", ",", "0"}], "}"}], ",", "perp"}], 
              "}"}], "]"}], "]"}]}], "}"}], "]"}]}], "\[IndentingNewLine]", 
       "\[IndentingNewLine]", "}"}], "]"}]}]}], "\[IndentingNewLine]", 
   "]"}]}]], "Input",
 CellChangeTimes->{{3.815213241032792*^9, 3.815213316161878*^9}, {
  3.815213384161564*^9, 3.815213416927949*^9}},
 CellLabel->"In[45]:=",ExpressionUUID->"d291f8bd-c10e-4f79-8ff3-b3b4c35fa47e"],

Cell[BoxData[
 Graphics3DBox[{
   {RGBColor[1, 0, 0], Arrowheads[0.05], 
    Arrow3DBox[TubeBox[{{-1, 0, 0}, {1, 0, 0}}]]}, 
   {RGBColor[0, 0, 1], Arrowheads[0.05], 
    Arrow3DBox[TubeBox[{{0, -1, 0}, {0, 1, 0}}]]}, 
   {RGBColor[1, 1, 0], Arrowheads[0.05], 
    Arrow3DBox[TubeBox[{{0, 0, -1}, {0, 0, 1}}]]}, 
   {GrayLevel[0], Arrowheads[0.05], 
    Arrow3DBox[TubeBox[{{0, 0, 0}, {2, 3, 1}}]]}, 
   {GrayLevel[0.5], Arrowheads[0.05], 
    Arrow3DBox[TubeBox[{{0, 0, 0}, {3, 4, -1}}]]}, 
   {RGBColor[1, 0.5, 0.5], Arrowheads[0.05], 
    Arrow3DBox[
     TubeBox[{{0, 0, 0}, {2.4285714285714284`, 3.642857142857143, 
      1.2142857142857142`}}]]}, 
   {RGBColor[0.87, 0.94, 1], Arrowheads[0.05], 
    Arrow3DBox[
     TubeBox[{{0, 0, 0}, {0.5714285714285714, 
      0.35714285714285715`, -2.2142857142857144`}}]]}},
  ImageSize->{360.0432578745785, 323.19565712961565`},
  ImageSizeRaw->Automatic,
  ViewPoint->{2.97377672947381, -0.2793993810466108, 1.590153435713302},
  ViewVertical->{0.1318271506446488, 0.006907255932992772, 
   0.9912486530474526}]], "Output",
 CellChangeTimes->{
  3.8152133276122713`*^9, {3.8152134029463506`*^9, 3.8152134175093913`*^9}},
 CellLabel->"Out[45]=",ExpressionUUID->"66454569-b3dd-4443-acc6-d04d8260642e"]
}, Open  ]]
},
WindowSize->{1192, 1181},
WindowMargins->{{261, Automatic}, {54, Automatic}},
PrintingPageRange->{Automatic, Automatic},
FrontEndVersion->"12.1 for Microsoft Windows (64-bit) (June 19, 2020)",
StyleDefinitions->"Default.nb",
ExpressionUUID->"51383c55-4c45-4746-9c2b-fd1d597cd1fa"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 9131, 225, 871, "Input",ExpressionUUID->"4fb5e836-44c1-496b-9fc1-5d8a1d36272b"],
Cell[9714, 249, 2070, 40, 457, "Output",ExpressionUUID->"53fe535e-9311-4347-aae1-c657d04065d8"]
}, Open  ]],
Cell[CellGroupData[{
Cell[11821, 294, 2996, 85, 67, "Input",ExpressionUUID->"a0b0cac9-69e6-4879-b1ea-d26afbd9bc35"],
Cell[14820, 381, 502, 11, 21, "Message",ExpressionUUID->"80b28fbb-e146-4c71-a61d-0f7e5d290dcd"],
Cell[15325, 394, 500, 11, 21, "Message",ExpressionUUID->"ede53229-aa87-4665-9947-70f9552a026c"],
Cell[15828, 407, 500, 11, 21, "Message",ExpressionUUID->"8e30217c-634f-4031-afb9-528fcc2b57c9"],
Cell[16331, 420, 501, 11, 21, "Message",ExpressionUUID->"42feae5f-bdd5-40e3-a210-a018a31811ee"],
Cell[16835, 433, 4588, 126, 85, "Output",ExpressionUUID->"c13c9f82-9ec8-4256-89a6-00ba09de7944"]
}, Open  ]],
Cell[21438, 562, 152, 3, 28, "Input",ExpressionUUID->"13feac70-d460-4b88-b187-4dc94c7d2f43"],
Cell[CellGroupData[{
Cell[21615, 569, 5392, 140, 639, "Input",ExpressionUUID->"d291f8bd-c10e-4f79-8ff3-b3b4c35fa47e"],
Cell[27010, 711, 1247, 27, 361, "Output",ExpressionUUID->"66454569-b3dd-4443-acc6-d04d8260642e"]
}, Open  ]]
}
]
*)


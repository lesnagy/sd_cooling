def float_to_str(x: float):
    r"""
    Formats a floating point number so that it is suitable to become part of
    a file name. Note: 'd' means decimal point, 'p' means plus
    """
    str_flt = "{:5.1e}".format(x)
    str_flt = str_flt.replace(".", "d")
    str_flt = str_flt.replace("+", "p")

    return str_flt

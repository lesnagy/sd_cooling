r"""
A useful collection of geometry functions.
"""

from math import sqrt, acos, cos, sin, pi


def fibonacci_sphere(n=1):
    r"""
    Sample points on a sphere according to Fibonacci spiral.
    :param n: the number of points.
    :return: a list of `n` points on a unit sphere.
    """
    points = []
    phi = (1.0 + sqrt(5.0)) / 2.0

    for i in range(1, n + 1):
        theta = (2.0 * pi * float(i)) / phi
        psi = acos(1 - ((2 * i) / float(n)) + (1 / float(n)))

        points.append((cos(theta) * sin(psi), sin(theta) * sin(psi), cos(psi)))

    return points


if __name__ == "__main__":
    points = fibonacci_sphere(30)
    for point in points:
        x = point[0]
        y = point[1]
        z = point[2]
        print("{:20.15f} {:20.15f} {:20.15f}".format(x, y, z))

r"""
Some utility functions that are useful when parsing command line options.
"""


def parse_size(size):
    r"""
    The size should either be a range or a single size.
    :param size: the size to parse, either an integer or a range.
    :return: a list of integers.
    """
    regex_single_size = re.compile(r"([0-9]+)")
    regex_range_size = re.compile(r"([0-9]+):([0-9]+):([0-9]+)")

    match_single_size = regex_single_size.fullmatch(size)
    if match_single_size:
        return [int(match_single_size.group(1))]

    match_range_size = regex_range_size.fullmatch(size)
    if match_range_size:
        start = int(match_range_size.group(1))
        step = int(match_range_size.group(2))
        stop = int(match_range_size.group(3)) + step
        return list(range(start, stop, step))


def parse_elong(elong):
    r"""
    The elongation should either be a range or a single elongation.
    :param elong: the elong to parse, either an integer or a range.
    :return: a list of integers.
    """
    regex_single_elong = re.compile(r"([0-9]+)")
    regex_range_elong = re.compile(r"([0-9]+):([0-9]+):([0-9]+)")

    match_single_elong = regex_single_elong.fullmatch(elong)
    if match_single_elong:
        return [int(match_single_elong.group(1))]

    match_range_elong = regex_range_elong.fullmatch(elong)
    if match_range_elong:
        start = int(match_range_elong.group(1))
        step = int(match_range_elong.group(2))
        stop = int(match_range_elong.group(3)) + step
        return list(range(start, stop, step))


def parse_float_list(lst_floats: str):
    r"""
    Parse a comma separated list of float values given as a string.
    :param lst_floats: a string of comma separated float values.
    :return: a list of float values.
    """
    # Remove spaces.
    str_floats = lst_floats.replace(" ", "")

    # Split on the comma.
    str_floats = str_floats.split(",")

    # Convert entries to floats (should throw exceptions on failure).
    return [float(v) for v in str_floats]

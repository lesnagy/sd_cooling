r"""
Utility functions to create data frames.
"""

import pandas as pd


def lst_to_dataframe(
        times, temperatures,
        pop_mags, pop_mags_para, pop_mags_perp,
        equi_mags, equi_mags_para, equi_mags_perp,
        pop_mags_ms=None, pop_mags_para_ms=None, pop_mags_perp_ms=None,
        equi_mags_ms=None, equi_mags_para_ms=None, equi_mags_perp_ms=None):
    r"""
    Colverts a set of raw data lists to a Pandas DataFrame object.
    :param times: a list containing times.
    :param temperatures: a list containing temperatures.
    :param pop_mags: a list containing fractional population magnetization.
    :param pop_mags_para: a list containing fractional population magnetization
                          in the direction of the applied field.
    :param pop_mags_perp: a list containing fractional population magnetization
                          perpendicular to the applied field.
    :param equi_mags: a list containing fractional equilibrium magnetization.
    :param equi_mags_para: a list containing fractional equilibrium
                           magnetization in the direction of the applied field.
    :param equi_mags_perp: a list containing fractional equilibrium
                           magnetization perpendicular to the applied field.
    :param pop_mags_ms: a list containing population magnetization.
    :param pop_mags_para_ms: a list containing population magnetization
                             in the direction of the applied field.
    :param pop_mags_perp_ms: a list containing population magnetization
                             perpendicular to the applied field.
    :param equi_mags_ms: a list containing equilibrium magnetization.
    :param equi_mags_para_ms: a list containing equilibrium magnetization in the
                              direction of the applied field.
    :param equi_mags_perp_ms: a list containing equilibrium magnetization in the
                              perpendicular to the applied field.
    :return: a Pandas DataFrame.
    """
    columns = [
        "time",
        "temperature",
        "M cooling (fractional)",
        "M cooling parallel (fractional)",
        "M cooling perpendicular (fractional)",
        "M equilibrium (fractional)",
        "M equilibrium parallel (fractional)",
        "M equilibrium perpendicular (fractional)",
    ]

    data = {
        columns[0]: times,
        columns[1]: temperatures,
        columns[2]: pop_mags,
        columns[3]: pop_mags_para,
        columns[4]: pop_mags_perp,
        columns[5]: equi_mags,
        columns[6]: equi_mags_para,
        columns[7]: equi_mags_perp
    }

    if pop_mags_ms is not None:
        columns.append("M cooling")
        data[columns[-1]] = pop_mags_ms
    if pop_mags_para_ms is not None:
        columns.append("M cooling parallel")
        data[columns[-1]] = pop_mags_para_ms
    if pop_mags_perp_ms is not None:
        columns.append("M cooling perpendicular")
        data[columns[-1]] = pop_mags_perp_ms

    if equi_mags_ms is not None:
        columns.append("M equilibrium")
        data[columns[-1]] = equi_mags_ms
    if equi_mags_para_ms is not None:
        columns.append("M equilibrium parallel")
        data[columns[-1]] = equi_mags_para_ms
    if equi_mags_perp_ms is not None:
        columns.append("M equilibrium perpendicular")
        data[columns[-1]] = equi_mags_perp_ms

    return pd.DataFrame(data,
                        columns=columns)

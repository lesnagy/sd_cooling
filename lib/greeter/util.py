def add(a, b):
    r"""
    A function that returns the sum of a & b - what could be more useful than
    that!
    :param a: a number .
    :param b: another number.
    :return: the sum of `a` and `b`.
    """
    return a + b
